-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2023 at 10:54 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fpanel_registrar`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `organization` varchar(255) DEFAULT NULL,
  `organization_type` int(3) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `admin_type_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `additional_details` text DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `remember_token` varchar(100) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `firstname`, `lastname`, `email`, `phone_number`, `email_verified_at`, `password`, `organization`, `organization_type`, `address`, `admin_type_id`, `image`, `status`, `additional_details`, `created_at`, `updated_at`, `remember_token`, `deleted_at`) VALUES
(1, 'Admin', NULL, 'admin@gmail.com', '60123593905', NULL, '$2a$12$xqW0YZ.AP5dhuOOmAOGliuMpVbHxxxkz2SW4DmORAjo50Jhhv8wW2', 'zooo', NULL, 'malaysia', 1, NULL, 0, 'mapoooooooooooo joooooooooooooo', '2023-02-27 17:43:52', '2023-03-15 20:08:55', NULL, NULL),
(6, 'ratha343', NULL, 'customer@gmail.com', '601235967850', NULL, '$2a$12$xqW0YZ.AP5dhuOOmAOGliuMpVbHxxxkz2SW4DmORAjo50Jhhv8wW2', '', NULL, NULL, 2, NULL, 0, NULL, '2023-03-14 19:52:54', '2023-03-16 09:17:03', NULL, NULL),
(7, 'gopi121', NULL, 'dealer@gmail.com', '601235784587', NULL, '$2a$12$xqW0YZ.AP5dhuOOmAOGliuMpVbHxxxkz2SW4DmORAjo50Jhhv8wW2', '', NULL, NULL, 3, NULL, 0, NULL, '2023-03-14 19:53:39', '2023-03-16 09:17:12', NULL, NULL),
(8, 'manikkam555', NULL, 'reseller@gmail.com', '60123598746', NULL, '$2a$12$xqW0YZ.AP5dhuOOmAOGliuMpVbHxxxkz2SW4DmORAjo50Jhhv8wW2', '', NULL, 'A-1-1, Jalan Multimedia 7/AH, I-City\n40000 Shah Alam, Selangor, Malaysia.', 4, NULL, 0, NULL, '2023-03-14 19:54:41', '2023-03-20 06:17:01', NULL, '2023-03-15 08:47:35');

-- --------------------------------------------------------

--
-- Table structure for table `admin_types`
--

CREATE TABLE `admin_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_types`
--

INSERT INTO `admin_types` (`id`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin', '0', '2023-02-17 05:06:59', '2023-03-21 02:44:11'),
(2, 'Customer', '0', '2023-02-24 04:52:14', '2023-03-21 02:45:13'),
(3, 'Dealer', '0', '2023-02-27 02:43:33', '2023-03-21 02:45:26'),
(4, 'Reseller', '0', '2023-02-24 04:52:14', '2023-03-21 02:45:45');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Table structure for table `domains`
--

CREATE TABLE `domains` (
  `id` int(11) NOT NULL,
  `domain_name` varchar(255) DEFAULT NULL,
  `registration_name` varchar(255) DEFAULT NULL,
  `registration_no` varchar(255) DEFAULT NULL,
  `registration_date` datetime DEFAULT current_timestamp(),
  `expire_date` datetime DEFAULT current_timestamp(),
  `domain_type` varchar(10) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `ic_number` varchar(255) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `postal_code` int(10) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `domains`
--

INSERT INTO `domains` (`id`, `domain_name`, `registration_name`, `registration_no`, `registration_date`, `expire_date`, `domain_type`, `full_name`, `address`, `email`, `phone`, `ic_number`, `dob`, `postal_code`, `city`, `state`, `country`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'abcd.com', 'abcd software limited', 'RE#00000', '2023-03-21 10:32:33', '2024-03-20 10:32:33', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 8, '2023-03-21 02:33:51', '2023-03-21 02:33:51'),
(2, 'abcd1.commm', 'abcd1 software limited', '000111', '2023-03-21 00:00:00', '2024-03-20 00:00:00', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 9, '2023-03-21 02:33:51', '2023-03-21 00:30:10');

-- --------------------------------------------------------

--
-- Table structure for table `domains_invoice_party`
--

CREATE TABLE `domains_invoice_party` (
  `id` int(11) NOT NULL,
  `domain_id` int(11) DEFAULT NULL,
  `invoice_party_id` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `domains_invoice_party`
--

INSERT INTO `domains_invoice_party` (`id`, `domain_id`, `invoice_party_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 0, '2023-03-21 00:25:52', '2023-03-21 00:26:16');

-- --------------------------------------------------------

--
-- Table structure for table `domains_nameserver`
--

CREATE TABLE `domains_nameserver` (
  `id` int(11) NOT NULL,
  `domain_id` int(11) DEFAULT NULL,
  `hostname1` text DEFAULT NULL,
  `hostname2` text DEFAULT NULL,
  `status` varchar(3) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `domains_nameserver`
--

INSERT INTO `domains_nameserver` (`id`, `domain_id`, `hostname1`, `hostname2`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'abcd11.com', 'abcd22.com', '0', '2023-03-20 22:51:56', '2023-03-20 22:56:01');

-- --------------------------------------------------------

--
-- Table structure for table `domains_type`
--

CREATE TABLE `domains_type` (
  `id` int(11) NOT NULL,
  `domain_type` varchar(255) DEFAULT NULL,
  `status` int(3) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `domains_type`
--

INSERT INTO `domains_type` (`id`, `domain_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Orgination', 0, '2023-03-21 02:26:26', '2023-03-21 02:26:26');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_party_list`
--

CREATE TABLE `invoice_party_list` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `invoice_party_list`
--

INSERT INTO `invoice_party_list` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'First online', 0, '2023-03-21 07:43:29', '2023-03-21 07:43:29'),
(2, 'First two', 0, '2023-03-21 07:43:29', '2023-03-21 07:43:29');

-- --------------------------------------------------------

--
-- Table structure for table `menu_lists`
--

CREATE TABLE `menu_lists` (
  `id` int(11) NOT NULL,
  `menu_name` text NOT NULL,
  `menu_url` text NOT NULL,
  `label_name` varchar(200) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `menu_lists`
--

INSERT INTO `menu_lists` (`id`, `menu_name`, `menu_url`, `label_name`, `created_at`, `updated_at`) VALUES
(19, 'Settings', 'settings', 'Settings', '2023-03-11 05:22:10', '2023-03-11 05:22:10'),
(20, 'Settings', 'smtpsettings', 'SMTP Settings', '2023-03-11 05:22:10', '2023-03-11 05:22:10'),
(21, 'Admin', 'admins', 'Admin Listing', '2023-03-11 05:24:16', '2023-03-11 05:24:16'),
(22, 'Admin', 'useractivity', 'Audit Trail', '2023-03-11 05:24:16', '2023-03-11 05:24:16'),
(23, 'Admin', 'loginhistroy', 'Login Histroy', '2023-03-11 05:25:30', '2023-03-11 05:25:30'),
(24, 'Admin', 'accesslevel', 'Admin Access Level', '2023-03-14 16:19:58', '2023-03-14 16:19:58');

-- --------------------------------------------------------

--
-- Table structure for table `payment_type`
--

CREATE TABLE `payment_type` (
  `id` int(11) NOT NULL,
  `domain_type` varchar(255) DEFAULT NULL,
  `status` int(3) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `payment_type`
--

INSERT INTO `payment_type` (`id`, `domain_type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Debit Card', 0, '2023-03-21 02:26:26', '2023-03-21 02:26:26');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `site_name` varchar(255) DEFAULT NULL,
  `site_link` text DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `meta_author` text DEFAULT NULL,
  `meta_keywords` text DEFAULT NULL,
  `meta_descriptions` text DEFAULT NULL,
  `header` text DEFAULT NULL,
  `logo` text DEFAULT NULL,
  `favicon` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `site_name`, `site_link`, `email`, `phone`, `fax`, `address`, `meta_author`, `meta_keywords`, `meta_descriptions`, `header`, `logo`, `favicon`, `created_at`, `updated_at`) VALUES
(1, 'registrar name', 'registrar link', 'mathan@gmail.com', '60127893901', '60129593901', 'malaysia', 'meta author', 'meta keyword', 'meta descriptions', NULL, '22CVIF1dSzB6cpPgBOtL69O15JdwF4oHUFTSaQAQ.png', 'GvYrYl1JlFf95mOo3WfGt3o0aG3MahVpeUtcNyK1.ico', '2023-03-14 19:32:54', '2023-03-15 00:17:12');

-- --------------------------------------------------------

--
-- Table structure for table `smtp_settings`
--

CREATE TABLE `smtp_settings` (
  `id` int(11) NOT NULL,
  `server_name` varchar(255) DEFAULT NULL,
  `port` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `password` text DEFAULT NULL,
  `from_name` varchar(255) DEFAULT NULL,
  `reply_email` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `smtp_settings`
--

INSERT INTO `smtp_settings` (`id`, `server_name`, `port`, `user_name`, `password`, `from_name`, `reply_email`, `created_at`, `updated_at`) VALUES
(1, 'smtp.gmail.com', '587', 'testhaneefa@gmail.com', 'fuyqrsencosxdfjb', 'registrar', 'no_reply@malaysiasms.my', '2023-03-14 19:35:35', '2023-03-14 19:35:35');

-- --------------------------------------------------------

--
-- Table structure for table `user_access_level`
--

CREATE TABLE `user_access_level` (
  `id` int(11) NOT NULL,
  `admin_type_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `permission_create` int(11) DEFAULT 0,
  `permission_update` int(11) DEFAULT 0,
  `permission_delete` int(11) DEFAULT 0,
  `permission_view` int(11) DEFAULT 0,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `user_access_level`
--

INSERT INTO `user_access_level` (`id`, `admin_type_id`, `menu_id`, `permission_create`, `permission_update`, `permission_delete`, `permission_view`, `created_at`, `updated_at`) VALUES
(1, 2, 19, 1, 0, 0, 0, '2023-03-15 04:19:54', '2023-03-15 04:19:54'),
(2, 3, 19, 0, 1, 0, 0, '2023-03-15 04:19:59', '2023-03-15 04:19:59'),
(3, 4, 19, 0, 0, 1, 0, '2023-03-15 04:20:00', '2023-03-15 04:20:00'),
(4, 2, 20, 0, 0, 1, 0, '2023-03-15 04:20:23', '2023-03-15 04:20:23');

-- --------------------------------------------------------

--
-- Table structure for table `user_activity`
--

CREATE TABLE `user_activity` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `contentId` int(11) DEFAULT NULL,
  `contentType` text DEFAULT NULL,
  `action` text DEFAULT NULL,
  `description` text DEFAULT NULL,
  `details` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user_activity`
--

INSERT INTO `user_activity` (`id`, `user_id`, `contentId`, `contentType`, `action`, `description`, `details`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'App\\Models\\Settings', 'create', 'Created a Settings', '{\"site_name\":\"registrar name\",\"site_link\":\"registrar link\",\"email\":\"mathan@gmail.com\",\"phone\":\"60123593901\",\"fax\":\"60123593901\",\"address\":\"malaysia\",\"meta_descriptions\":\"meta descriptions\",\"meta_keywords\":\"meta keyword\",\"meta_author\":\"meta author\",\"updated_at\":\"2023-03-15 03:32:54\",\"created_at\":\"2023-03-15 03:32:54\",\"id\":1}', '2023-03-14 19:32:54', '2023-03-14 19:32:54'),
(2, 1, 1, 'App\\Models\\Settings', 'update', 'Updated a Settings', '{\"phone\":\"60127893901\",\"fax\":\"60129593901\",\"updated_at\":\"2023-03-15 03:34:30\"}', '2023-03-14 19:34:30', '2023-03-14 19:34:30'),
(3, 1, 1, 'App\\Models\\SmtpSettings', 'create', 'Created a SmtpSettings', '{\"server_name\":\"server name\",\"port\":\"port\",\"user_name\":\"user name\",\"password\":\"password\",\"from_name\":\"from name\",\"reply_email\":\"reply email\",\"updated_at\":\"2023-03-15 03:35:35\",\"created_at\":\"2023-03-15 03:35:35\",\"id\":1}', '2023-03-14 19:35:35', '2023-03-14 19:35:35'),
(4, 1, 6, 'App\\Models\\Admin', 'create', 'Created a Admin', '{\"firstname\":\"ratha343\",\"number\":\"601235967850\",\"email\":\"ratha54343@gmail.com\",\"admin_type_id\":\"2\",\"status\":0,\"updated_at\":\"2023-03-15 03:52:54\",\"created_at\":\"2023-03-15 03:52:54\",\"id\":6}', '2023-03-14 19:52:54', '2023-03-14 19:52:54'),
(5, 1, 7, 'App\\Models\\Admin', 'create', 'Created a Admin', '{\"firstname\":\"gopi121\",\"number\":\"601235784587\",\"email\":\"gopi121@gmail.com\",\"admin_type_id\":\"3\",\"status\":0,\"updated_at\":\"2023-03-15 03:53:39\",\"created_at\":\"2023-03-15 03:53:39\",\"id\":7}', '2023-03-14 19:53:39', '2023-03-14 19:53:39'),
(6, 1, 8, 'App\\Models\\Admin', 'create', 'Created a Admin', '{\"firstname\":\"manikkam555\",\"number\":\"60123598746\",\"email\":\"manikkam555@gmail.com\",\"admin_type_id\":\"4\",\"status\":0,\"updated_at\":\"2023-03-15 03:54:41\",\"created_at\":\"2023-03-15 03:54:41\",\"id\":8}', '2023-03-14 19:54:41', '2023-03-14 19:54:41'),
(7, 1, 1, 'App\\Models\\Settings', 'update', 'Updated a Settings', '{\"logo\":\"sgdK6RHDwNPnh1JEum17rYYG3SHaqmLPQQjLWM0w.png\",\"updated_at\":\"2023-03-15 08:01:39\"}', '2023-03-15 00:01:39', '2023-03-15 00:01:39'),
(8, 1, 1, 'App\\Models\\Settings', 'update', 'Updated a Settings', '{\"logo\":\"WkRJo6M6kbEYoFddZK2lRCED2WCLueevFpLsmzSY.png\",\"updated_at\":\"2023-03-15 08:08:34\"}', '2023-03-15 00:08:34', '2023-03-15 00:08:34'),
(9, 1, 1, 'App\\Models\\Settings', 'update', 'Updated a Settings', '{\"logo\":\"22CVIF1dSzB6cpPgBOtL69O15JdwF4oHUFTSaQAQ.png\",\"updated_at\":\"2023-03-15 08:10:58\"}', '2023-03-15 00:10:58', '2023-03-15 00:10:58'),
(10, 1, 1, 'App\\Models\\Settings', 'update', 'Updated a Settings', '{\"favicon\":\"GvYrYl1JlFf95mOo3WfGt3o0aG3MahVpeUtcNyK1.ico\",\"updated_at\":\"2023-03-15 08:17:12\"}', '2023-03-15 00:17:12', '2023-03-15 00:17:12'),
(11, 1, 7, 'App\\Models\\Admin', 'update', 'Updated a Admin', '{\"password\":\"$2y$10$4j0vZ7K5CxPpqhW4fJZ1j.\\/Ibbr\\/O638JNpN1Oak05o8Rtvg7dDaO\",\"updated_at\":\"2023-03-15 08:34:06\"}', '2023-03-15 00:34:06', '2023-03-15 00:34:06'),
(12, 1, 7, 'App\\Models\\Admin', 'update', 'Updated a Admin', '{\"password\":\"$2y$10$UXeyjz1xJgjf1niOWgRhK.WJfqa2j7ocsQ.ZQ7yD7m7zLOJ8ZBsMe\",\"updated_at\":\"2023-03-15 08:38:31\"}', '2023-03-15 00:38:31', '2023-03-15 00:38:31'),
(13, 1, 7, 'App\\Models\\Admin', 'update', 'Updated a Admin', '{\"password\":\"$2y$10$UwCLbo0tIQNS5\\/p87yqcgehAOWeRdbtHXJi.Dj.rKMUHwFLK5oCFG\",\"updated_at\":\"2023-03-15 08:39:09\"}', '2023-03-15 00:39:09', '2023-03-15 00:39:09'),
(14, 1, 6, 'App\\Models\\Admin', 'update', 'Updated a Admin', '{\"password\":\"$2y$10$wHdBGDRZfWUQpMDl0MQxaOAn\\/qZIfewxH1cfaSNVmGTAE0LUM7aq.\",\"updated_at\":\"2023-03-15 08:43:43\"}', '2023-03-15 00:43:43', '2023-03-15 00:43:43'),
(15, 1, 6, 'App\\Models\\Admin', 'update', 'Updated a Admin', '{\"password\":\"$2y$10$CY3ZPvhmnmsxgGjBng0h4uNCqlwLRI47sr8UnaDgWvL93vx.sovOW\",\"updated_at\":\"2023-03-15 08:46:24\"}', '2023-03-15 00:46:24', '2023-03-15 00:46:24'),
(16, 1, 6, 'App\\Models\\Admin', 'update', 'Updated a Admin', '{\"password\":\"$2y$10$bAn\\/lxJZv7NmF1cqSdZQpOmJVSCak3S06JmdfKnnUEnA01djeVxiK\",\"updated_at\":\"2023-03-15 08:46:36\"}', '2023-03-15 00:46:36', '2023-03-15 00:46:36'),
(17, 1, 8, 'App\\Models\\Admin', 'update', 'Updated a Admin', '{\"updated_at\":\"2023-03-15 08:47:35\",\"deleted_at\":\"2023-03-15T08:47:35.855396Z\"}', '2023-03-15 00:47:35', '2023-03-15 00:47:35'),
(18, NULL, 6, 'App\\Models\\Admin', 'update', 'Updated a Admin', '{\"password\":\"$2y$10$\\/x4gqFaquJQe5NsJwH9Sd.0zpx5MoYmseHMx3jEV7TwtfqIDqH9HS\",\"updated_at\":\"2023-03-15 08:52:39\"}', '2023-03-15 00:52:39', '2023-03-15 00:52:39'),
(19, 1, 1, 'App\\Models\\Admin', 'update', 'Updated a Admin', '{\"organization\":\"zooo\",\"address\":\"malaysia\",\"additional_details\":\"mapoooooooooooo joooooooooooooo\",\"updated_at\":\"2023-03-16 04:08:55\"}', '2023-03-15 20:08:55', '2023-03-15 20:08:55');

-- --------------------------------------------------------

--
-- Table structure for table `user_login_history`
--

CREATE TABLE `user_login_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_login_at` datetime DEFAULT NULL,
  `last_login_ip` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user_login_history`
--

INSERT INTO `user_login_history` (`id`, `user_id`, `last_login_at`, `last_login_ip`, `created_at`, `updated_at`) VALUES
(1, 1, '2023-03-15 03:05:08', '127.0.0.1', '2023-03-14 19:05:08', '2023-03-14 19:05:08'),
(2, 6, '2023-03-15 04:02:49', '127.0.0.1', '2023-03-14 20:02:49', '2023-03-14 20:02:49'),
(3, 7, '2023-03-15 04:03:33', '127.0.0.1', '2023-03-14 20:03:33', '2023-03-14 20:03:33'),
(4, 1, '2023-03-15 04:16:21', '127.0.0.1', '2023-03-14 20:16:21', '2023-03-14 20:16:21'),
(5, 1, '2023-03-15 04:30:39', '127.0.0.1', '2023-03-14 20:30:39', '2023-03-14 20:30:39'),
(6, 1, '2023-03-15 04:30:52', '127.0.0.1', '2023-03-14 20:30:52', '2023-03-14 20:30:52'),
(7, 1, '2023-03-15 06:46:54', '127.0.0.1', '2023-03-14 22:46:54', '2023-03-14 22:46:54'),
(8, 1, '2023-03-15 06:53:28', '127.0.0.1', '2023-03-14 22:53:28', '2023-03-14 22:53:28'),
(9, 1, '2023-03-15 07:33:34', '127.0.0.1', '2023-03-14 23:33:34', '2023-03-14 23:33:34'),
(10, 1, '2023-03-15 08:43:23', '127.0.0.1', '2023-03-15 00:43:23', '2023-03-15 00:43:23'),
(11, 1, '2023-03-15 08:53:49', '127.0.0.1', '2023-03-15 00:53:49', '2023-03-15 00:53:49'),
(12, 1, '2023-03-15 10:00:40', '127.0.0.1', '2023-03-15 02:00:40', '2023-03-15 02:00:40'),
(13, 1, '2023-03-16 01:11:02', '127.0.0.1', '2023-03-15 17:11:02', '2023-03-15 17:11:02'),
(14, 1, '2023-03-16 07:59:37', '127.0.0.1', '2023-03-15 23:59:37', '2023-03-15 23:59:37'),
(15, 6, '2023-03-16 09:17:21', '127.0.0.1', '2023-03-16 01:17:21', '2023-03-16 01:17:21'),
(16, 1, '2023-03-20 01:18:40', '127.0.0.1', '2023-03-19 17:18:40', '2023-03-19 17:18:40'),
(17, 8, '2023-03-20 01:24:26', '127.0.0.1', '2023-03-19 17:24:26', '2023-03-19 17:24:26'),
(18, 7, '2023-03-20 01:31:43', '127.0.0.1', '2023-03-19 17:31:43', '2023-03-19 17:31:43'),
(19, 1, '2023-03-21 01:12:28', '127.0.0.1', '2023-03-20 17:12:28', '2023-03-20 17:12:28'),
(20, 8, '2023-03-21 01:18:03', '127.0.0.1', '2023-03-20 17:18:03', '2023-03-20 17:18:03'),
(21, 6, '2023-03-21 03:08:25', '127.0.0.1', '2023-03-20 19:08:25', '2023-03-20 19:08:25'),
(22, 1, '2023-03-21 03:08:52', '127.0.0.1', '2023-03-20 19:08:52', '2023-03-20 19:08:52'),
(23, 8, '2023-03-21 03:14:24', '127.0.0.1', '2023-03-20 19:14:24', '2023-03-20 19:14:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_types`
--
ALTER TABLE `admin_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domains`
--
ALTER TABLE `domains`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domains_invoice_party`
--
ALTER TABLE `domains_invoice_party`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domains_nameserver`
--
ALTER TABLE `domains_nameserver`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `domains_type`
--
ALTER TABLE `domains_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_party_list`
--
ALTER TABLE `invoice_party_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_lists`
--
ALTER TABLE `menu_lists`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_type`
--
ALTER TABLE `payment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smtp_settings`
--
ALTER TABLE `smtp_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_level`
--
ALTER TABLE `user_access_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_activity`
--
ALTER TABLE `user_activity`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_login_history`
--
ALTER TABLE `user_login_history`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `admin_types`
--
ALTER TABLE `admin_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `domains`
--
ALTER TABLE `domains`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `domains_invoice_party`
--
ALTER TABLE `domains_invoice_party`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `domains_nameserver`
--
ALTER TABLE `domains_nameserver`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `domains_type`
--
ALTER TABLE `domains_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `invoice_party_list`
--
ALTER TABLE `invoice_party_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `menu_lists`
--
ALTER TABLE `menu_lists`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `payment_type`
--
ALTER TABLE `payment_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `smtp_settings`
--
ALTER TABLE `smtp_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_access_level`
--
ALTER TABLE `user_access_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_activity`
--
ALTER TABLE `user_activity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user_login_history`
--
ALTER TABLE `user_login_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
