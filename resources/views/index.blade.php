<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Bumilink Logistic</title>
<!-- Stylesheets -->
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
<link rel="icon" href="images/favicon.png" type="image/x-icon">

<!-- <link href="/asset/fontawesome6-2-0/css/all.css" rel="stylesheet"> -->
<!-- <link href="/asset/fontawesome6-2-0/css/brands.css" rel="stylesheet">
<link href="/asset/fontawesome6-2-0/css/solid.css" rel="stylesheet"> -->

<!-- Responsive -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->
<!--[if lt IE 9]><script src="js/respond.js"></script><![endif]-->
</head>

<body class="hidden-bar-wrapper">

<div class="page-wrapper">

    <!-- Preloader -->
    <div class="preloader"></div>

 	<!-- Main Header / Header Style Two -->
    <header class="main-header header-style-two">
			<div class="header-auto-container">
			<div class="header-inner">
				<!--Header Top-->
				<div class="header-top">

					<div class="clearfix">

						<!--Top Right-->
						<div class="top-right">

							<!-- Right List -->
							<ul class="right-list">
								<li><span class="icon flaticon-mail"></span><a href="mailto:cs@bumilink.com.my">cs@bumilink.com.my</a></li>
								<li><span class="icon flaticon-phone-contact"></span><a href="tel:+60351225355">+ 603 51225355</a></li>
								<li><span class="icon flaticon-fax"></span><a href="tel:+60351226355">+ 603 51226355</a></li>
							</ul> 

							<!--Social Box-->
							<ul class="social-box">
								<li><a href="#"><span class="fa fa-twitter"></span></a></li>
								<li><a href="#"><span class="fa fa-facebook"></span></a></li>
								<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
								<li><a href="#"><span class="fa fa-google-plus"></span></a></li>
							</ul>

						</div>

					</div>

				</div>

				<!--Header-Upper-->
				<div class="header-upper">
					<div class="auto-container">
					<div class="clearfix">

						<div class="pull-left logo-box">
							<div class="logo"><a href="index.html"><img src="images/bumilink-logo.png" alt="" title=""></a></div>
						</div>

						<div class="pull-right upper-right">

							<!--Header Lower-->
							<div class="header-lower"> 

									<div class="nav-outer clearfix">
										<!-- Main Menu -->
										<nav class="main-menu navbar-expand-md">
											<div class="navbar-header">
												<!-- Toggle Button -->
												<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
												</button>
											</div>

											<div class="navbar-collapse collapse clearfix" id="navbarSupportedContent">
												<ul class="navigation clearfix">
													<li class="current"><a href="index.html">Home</i></a> <!-- <li class="current dropdown"><a href="#">Home<i class="fa fa-angle-down"></i></a> 
														<ul> 
															<li><a href="#">Home Page 01</a></li> 
															<li><a href="index-2.html">Home Page 02</a></li>
														   <li class="dropdown"><a href="#">Header Styles</a>
															   <ul>
																   <li><a href="#">Header Style 01</a></li>
																   <li><a href="index-2.html">Header Style 02</a></li>
															   </ul>
														   </li> 
													   </ul> -->
												   </li>
												   <li><a href="about.html">About Us</a></li> 
												   <li class="dropdown">
													   <a href="services.html">Our Services<i class="fa fa-angle-down"></i></a>
													   <ul> 
														   <li class="dropdown"><a href="domesticservices.html">Domestic Services</a>
															   <ul>
																<li><a href="domesticservices.html#linkedsameday">Linked Same Day</a></li>
																<li><a href="domesticservices.html#linkedovernight">Linked Overnight</a></li>
																<li><a href="domesticservices.html#linkedeconomy">Linked Economy</a></li>
																<li><a href="domesticservices.html#transportation">Transportation (LTL/FTL)</a></li>
																<li><a href="domesticservices.html#homedeliveries">Home Deliveries & e-Commerces </a></li>
																<li><a href="domesticservices.html#retailmerchandising">Retail Merchandising
																</a></li>
															   </ul>
														   </li> 
														   <li class="dropdown"><a href="international.html">International</a>
															   <ul>
																<li><a href="international.html#globalsameday">Global Same Day
																</a></li>
																<li><a href="international.html#globalpackageexpress">Global Package Express</a></li>
																<li><a href="international.html#globalexpeditedfreight">Global Expedited Freight
																</a></li>
																<li><a href="international.html#globaleconomyfreight">Global Economy Freight 
																</a></li>
																<li><a href="international.html#globaloceanfreightlclfcl">Global Ocean Freight (LCL/FCL)
																</a></li>
																<li><a href="international.html#globalimportfreightrp">Global Import Freight R/P
																</a></li>
																<li><a href="international.html#crossborderexpress">Cross Border Express
																</a></li>
															   </ul>
														   </li> 
														   <li class="dropdown"><a href="globallogistic.html">Global Logistic</a>
															   <ul>
																   <li><a href="globallogistic.html#services">Services</a></li>
																   <li><a href="globallogistic.html#contractwarehousinganddistributionservice">Contract Warehousing and Distribution Service
																   </a></li>
																   <li><a href="globallogistic.html#corporatedistributionmanagement">Corporate Distribution Management
																   </a></li>
																   <li><a href="globallogistic.html#importinventorymanagement">Import Inventory Management
																   </a></li>
															   </ul>
														   </li> 
														   <li><a href="custombrokerage.html">Custom Brokerage</a></li> 
                                                           <li><a href="cargoinsurance.html">Cargo Insurances</a></li> 
                                                        </ul>
								                    <li class="dropdown"><a href="solutions.html">Solutions<i class="fa fa-angle-down"></i></a> 
									                <ul>
										                <li><a href="apparel.html">Apparel</a></li>
										                <li><a href="aviation.html">Aviation</a></li>
										                <li><a href="banking.html">Banking/Finance</a></li>
										                <li><a href="ecommerce.html">E-Commerce</a></li>
										                <li><a href="fmcg.html">FMCG</a></li>
										                <li><a href="healthcaremedicallogistics.html">Healthcare/Medical Logistics</a></li>
										                <li><a href="manufacturinglogistics.html">Manufacturing Logistics           </a></li>
										                <li><a href="technology.html">Technology</a></li>
										                <li><a href="customstaxconsultancy.html">Customs Tax Consultancy</a></li>
										                <li><a href="dedicatedfleetlogistics.html">Dedicated Fleet Logistics </a></li>
										                <li><a href="supplychainmanagement.html">Supply Chain Management </a></li>
										                <li><a href="wms.html">WMS </a></li> 
                                                    </ul>
								                </li>
								                    <li><a href="contact.html">Contact</a> 
									                <li><a href="getaquote.html">Get A Quote</a> 
                                                </li> 
												</ul>
											</div>
										</nav>

										<!-- Main Menu End-->
										<div class="outer-box clearfix">

											<!--Option Box-->
											<div class="option-box">

												<!--Search Box-->
												<div class="search-box-outer">
													<div class="dropdown">
														<button class="search-box-btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="flaticon-route"></span></button>
														<ul class="dropdown-menu pull-right search-panel" aria-labelledby="dropdownMenu3">
															<li class="panel-outer">
																<div class="form-container">
																	<form method="post" action="blog.html">
																		<div class="form-group">
																			<input type="search" name="field-name" value="" placeholder="Track Your Shipment" required>
																			<button type="submit" class="search-btn"><span class="fa fa-search"></span></button>
																		</div>
																	</form>
																</div>
															</li>
														</ul>
													</div>
												</div>

											</div>
										</div>
									</div>

							</div>
							<!--End Header Lower-->

						</div>

					</div>

				</div>
			<!--End Header Upper-->
			</div>
			</div>
        </div>

		<!--Sticky Header-->
        <div class="sticky-header">
        	<div class="auto-container clearfix">
            	<!--Logo-->
            	<div class="logo pull-left">
                	<a href="index.html" class="img-responsive"><img src="images/bumilink-logo-small.png" alt="" title=""></a>
                </div>

                <!--Right Col-->
                <div class="right-col pull-right">
                	<!-- Main Menu -->
                    <nav class="main-menu navbar-expand-md">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent1" aria-controls="navbarSupportedContent1" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <div class="navbar-collapse collapse clearfix" id="navbarSupportedContent1">
                            <ul class="navigation clearfix">
								<li class="current"><a href="index.html">Home</i></a> <!-- <li class="current dropdown"><a href="#">Home<i class="fa fa-angle-down"></i></a> 
									<ul> 
										<li><a href="index.html">Home Page 01</a></li> 
										<li><a href="index-2.html">Home Page 02</a></li>
									   <li class="dropdown"><a href="#">Header Styles</a>
										   <ul>
											   <li><a href="index.html">Header Style 01</a></li>
											   <li><a href="index-2.html">Header Style 02</a></li>
										   </ul>
									   </li> 
								   </ul> -->
							   </li>
							   <li><a href="about.html">About Us</a></li>
							   <div class="navbar-collapse collapse clearfix" id="navbarSupportedContent">
								   <ul class="navigation clearfix"> 
								   <li class="dropdown">
								   <a href="services.html">Our Services<i class="fa fa-angle-down"></i></a>
								   <ul> 
									   <li class="dropdown"><a href="domesticservices.html">Domestic Services</a>
										   <ul>
											   <li><a href="domesticservices.html#linkedsameday">Linked Same Day</a></li>
											   <li><a href="domesticservices.html#linkedovernight">Linked Overnight</a></li>
											   <li><a href="domesticservices.html#linkedeconomy">Linked Economy</a></li>
											   <li><a href="domesticservices.html#transportation">Transportation (LTL/FTL)</a></li>
											   <li><a href="domesticservices.html#homedeliveries">Home Deliveries & e-Commerces </a></li>
											   <li><a href="domesticservices.html#retailmerchandising">Retail Merchandising
											   </a></li>
										   </ul>
									   </li> 
									   <li class="dropdown"><a href="international.html">International</a>
                                        <ul>
                                            <li><a href="international.html#globalsameday">Global Same Day
                                            </a></li>
                                            <li><a href="international.html#globalpackageexpress">Global Package Express</a></li>
                                            <li><a href="international.html#globalexpeditedfreight">Global Expedited Freight
                                            </a></li>
                                            <li><a href="international.html#globaleconomyfreight">Global Economy Freight 
                                            </a></li>
                                            <li><a href="international.html#globaloceanfreightlclfcl">Global Ocean Freight (LCL/FCL)
                                            </a></li>
                                            <li><a href="international.html#globalimportfreightrp">Global Import Freight R/P
                                            </a></li>
                                            <li><a href="crossborderexpress.html">Cross Border Express
                                            </a></li>
                                        </ul>
                                    </li> 
									   <li class="dropdown"><a href="globallogistic.html">Global Logistic</a>
										   <ul>
											<li><a href="globallogistic.html#services">Services</a></li>
											<li><a href="globallogistic.html#contractwarehousinganddistributionservice">Contract Warehousing and Distribution Service
											</a></li>
											<li><a href="globallogistic.html#corporatedistributionmanagement">Corporate Distribution Management
											</a></li>
											<li><a href="globallogistic.html#importinventorymanagement">Import Inventory Management
											</a></li>
										   </ul>
									   </li> 
									   <li><a href="custombrokerage.html">Custom Brokerage</a></li>
									   <li><a href="cargoinsurance.html">Cargo Insurances</a></li> 
								   </ul>
								   <li class="dropdown"><a href="solutions.html">Solutions<i class="fa fa-angle-down"></i></a> 
									   <ul>
										   <li><a href="apparel.html">Apparel</a></li>
										   <li><a href="aviation.html">Aviation</a></li>
										   <li><a href="banking.html">Banking/Finance</a></li>
										   <li><a href="ecommerce.html">E-Commerce</a></li>
										   <li><a href="fmcg.html">FMCG</a></li>
										   <li><a href="healthcaremedicallogistics.html">Healthcare/Medical Logistics</a></li>
										   <li><a href="manufacturinglogistics.html">Manufacturing Logistics </a></li>
										   <li><a href="technology.html">Technology</a></li>
										   <li><a href="customstaxconsultancy.html">Customs Tax Consultancy</a></li>
										   <li><a href="dedicatedfleetlogistics.html">Dedicated Fleet Logistics </a></li>
										   <li><a href="supplychainmanagement.html">Supply Chain Management </a></li>
										   <li><a href="wms.html">WMS </a></li>
									   </ul>
								   </li>
								   <li><a href="contact.html">Contact</a> 
									   <li><a href="getaquote.html">Get A Quote</a> 
								   </li>
                            </ul>
                        </div>
                    </nav><!-- Main Menu End-->
                </div>
            </div>
        </div>
        <!--End Sticky Header-->

    </header>
    <!--End Main Header -->



	<!-- Banner Section Two -->
    <section class="banner-section-two" style="background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('images/background/home-banner.png');">
		<div class="auto-container">

			<!-- Content Column -->
			<div class="content-column">
				<div class="inner-column wow slideInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
					<h1>YOUR FULLFILMENT PARTNER</h1>
					<div class="text">Join the millions getting bargain deals offers ocean, air, and land freight <br>along with integrated supply chain services in more than 100 countries.</div>

					<!-- Banner Form -->
					<div class="banner-form-two wow slideInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
						<form method="post" action="index.html">
							<div class="row clearfix">

								<!-- Form Group -->
								<div class="form-group col-lg-4 col-md-6 col-sm-12">
									<input type="text" name="emailid" placeholder="Enter your Email ID" required>
								</div>

								<!-- Form Group -->
								<div class="form-group col-lg-4 col-md-6 col-sm-12">
									<input type="text" name="trackid" placeholder="Enter Your Track ID" required>
								</div>

								<!-- Form Group -->
								<div class="form-group col-lg-4 col-md-12 col-sm-12">
									<button type="submit" class="theme-btn btn-style-three">Search Now</button>
								</div>

							</div>
						</form>
					</div>
					<!-- End Banner Form -->

				</div>
			</div>

		</div>
	</section>
	<!-- End Banner Section Two -->

	<!-- Featured Section -->
	<section class="featured-section">
		<div class="outer-container">
			<div class="clearfix">

				<!-- Feature Block -->
				<div class="feature-block">
					<div class="inner-box">
						<div class="icon-box">
							<span class="icon flaticon-air-freight"></span>
						</div>
						<h3><a href="#">Air Freight</a></h3>
						<div class="text">Property Carried in Aircraft</div>
					</div>
				</div>

				<!-- Feature Block -->
				<div class="feature-block">
					<div class="inner-box">
						<div class="icon-box">
							<span class="icon flaticon-moving-truck"></span>
						</div>
						<h3><a href="#">Road Freight</a></h3>
						<div class="text">Motor Vehicles Transport Cargo</div>
					</div>
				</div>

				<!-- Feature Block -->
				<div class="feature-block">
					<div class="inner-box">
						<div class="icon-box">
							<span class="icon flaticon-boat"></span>
						</div>
						<h3><a href="#">Sea Freight</a></h3>
						<div class="text">Goods Carrier Ships</div>
					</div>
				</div>

				<!-- Feature Block -->
				<div class="feature-block">
					<div class="inner-box">
						<div class="icon-box">
							<span class="icon flaticon-planet-earth"></span>
						</div>
						<h3><a href="#">Insurance</a></h3>
						<div class="text">Protect Your Cargo</div>
					</div>
				</div>

			</div>
		</div>
	</section>
	<!-- End Featured Section -->

	<!-- Welcome Section -->
	<section class="welcome-section">
		<div class="auto-container">
			<div class="row clearfix">

				<!-- Content Column -->
				<div class="content-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="sec-title-two sec-title">
							<h2>Welcome To <span>Bumilink</span></h2>
							<div class="separater"></div>
						</div>
						<ul class="list-style-one">
							<li><span>Delivers fully integrated logistics solutions</span> We tailored to the needs of our customers and is now recognized as one of the leading logistics providers in the market.</li>
							<li><span>Diverse range of logistics services</span>Includes Global Warehousing & Distribution, Global Freight Management, Global Ocean Freight, International Network Solutions, Multimodal Transport Operator, Multicountry Consolidation, Supply Chain Consulting and Total Integrated Logistics Management Software System.</li>
							<li><span>Cargo Insurance</span>When you need to submit a claim, Bumilink makes sure that it is processed quickly and that you receive proper settlement with minimum delays.</li>
						</ul>
						<a href="about.html" class="theme-btn btn-style-four">Read More</a>
					</div>
				</div>

				<!-- Image Column -->
				<div class="image-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column wowallow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image">
							<img src="images/resource/home-image01.png" alt=""/>
						</div>
					</div>
				</div>

			</div>
		</div>
	</section>
	<!-- End Welcome Section -->

	<!-- Services Section Two -->
	<section class="services-section-two">
		<div class="auto-container">
			<div class="sec-title centered">
				<h3 style="font-family: Raleway;">Our <span> Services</span></h3>
				<div class="separater"></div>
			</div>
			<div class="row clearfix">
				<!-- Services Block -->
				<div class="services-block-three col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image"> 
							<a href="domesticservices.html">
							<img src="images/resource/home-service-1.png" alt=""/>
							<h4>Domestic Services</h4></a>
						</div>
					</div>
				</div>

				<!-- Services Block -->
				<div class="services-block-three col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image">
							<a href="international.html">
							<img src="images/resource/home-service-2.png" alt="" />
							<h4>International</h4></a>
						</div>
					</div>
				</div>

				<!-- Services Block -->
				<div class="services-block-three col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image">
							<a href="globallogistic.html">
							<img src="images/resource/home-service-3.png" alt="" />
							<h4>Global Logistics</h4></a>
						</div>
					</div>
				</div>

				<!-- Services Block -->
				<div class="services-block-three col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image">
							<a href="custombrokerage.html">
							<img src="images/resource/home-service-4.png" alt="" />
							<h4>Customs Brokerage</h4></a>
						</div>
					</div>
				</div>

				<!-- Services Block -->
				<div class="services-block-three col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box wow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="image">
							<a href="cargoinsurance.html">
							<img src="images/resource/home-service-5.png" alt="" />
							<h4>Cargo Insurance</h4></a>
						</div>
					</div>
				</div> 
			</div>
		</div>
	</section>
	<!-- End Services Section Two -->

	<!-- Counter Section -->
	<section class="counter-section" style="background-image:url(images/background/home-banner-counter.png)">
		<div class="auto-container">
			<div class="row clearfix">

				<!-- Order Column -->
				<div class="order-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column" style="background-image:url(images/background/4.png);">
						<div class="icon-box">
							<span class="flaticon flaticon-fast-delivery"></span>
						</div>
						<h2>Track Your Order</h2>
						<div class="text">Enter your Track Id For Instant Search</div>

						<!--Track Form-->
						<div class="track-form">
                            <form method="post" action="contact.html">
                                <div class="form-group">
                                    <input type="text" name="text" value="" placeholder="Tracking ID" required="">
                                    <button type="submit" class="theme-btn"><span class="fa fa-search"></span></button>
                                </div>
                            </form>
                        </div>

						<!-- Social Box -->
						<div class="social-box">
							<a href="#" class="fa fa-twitter"></a>
							<a href="#" class="fa fa-facebook"></a>
							<a href="#" class="fa fa-linkedin"></a>
							<a href="#" class="fa fa-google-plus"></a>
						</div>

					</div>
				</div>

				<!-- Counter Column -->
				<div class="counter-column col-lg-6 col-md-12 col-sm-12">
					<div class="inner-column">

						<div class="fact-counter">
							<div class="clearfix">
								<!--Column-->
								<div class="column counter-column col-lg-6 col-md-6 col-sm-12">
									<div class="inner">
										<div class="content">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2000" data-stop="60">0</span>
											</div>
											<h4 class="counter-title">Years Experience</h4>
										</div>
									</div>
								</div>

								<!--Column-->
								<div class="column counter-column col-lg-6 col-md-6 col-sm-12">
									<div class="inner">
										<div class="content">
											<div class="count-outer count-box alternate">
												<span class="count-text" data-speed="2800" data-stop="2500">0</span>+
											</div>
											<h4 class="counter-title">Professional Workers</h4>
										</div>
									</div>
								</div>

								<!--Column-->
								<div class="column counter-column col-lg-6 col-md-6 col-sm-12">
									<div class="inner">
										<div class="content">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2500" data-stop="80">0</span>%
											</div>
											<h4 class="counter-title">Areas Covered</h4>
										</div>
									</div>
								</div>

								<!--Column-->
								<div class="column counter-column col-lg-6 col-md-6 col-sm-12">
									<div class="inner">
										<div class="content">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2000" data-stop="205">0</span>+
											</div>
											<h4 class="counter-title">Countries Covered</h4>
										</div>
									</div>
								</div>

								<!--Column-->
								<div class="column counter-column col-lg-6 col-md-6 col-sm-12">
									<div class="inner">
										<div class="content">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2000" data-stop="180">0</span>+
											</div>
											<h4 class="counter-title">Corporate Clients</h4>
										</div>
									</div>
								</div>

								<!--Column-->
								<div class="column counter-column col-lg-6 col-md-6 col-sm-12">
									<div class="inner">
										<div class="content">
											<div class="count-outer count-box">
												<span class="count-text" data-speed="2000" data-stop="450">0</span>+
											</div>
											<h4 class="counter-title">Owned Vehicles</h4>
										</div>
									</div>
								</div>

							</div>
						</div>


					</div>
				</div>

			</div>
		</div>
	</section>

	<section class="team-section gap">
            <div class="auto-container">
			<div class="sec-title centered">
				<h3>Vision and<span> Mission</span></h3>
				<div class="separater"></div>
			</div>
			<div class="tem-sec">
				<div class="row">
					<div class="col-lg-6 col-md-12 col-sm-12">
						<div class="tm-bx">
							<div class="tm-thmb">
								<img src="images/resource/home-vision.png" style="padding-bottom: 5%;"alt="Team-Image.jpg"></img> 
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-12 col-sm-12">
						<div class="tm-bx">
							<div class="tm-thmb">
								<div class="tm-inf wowallow fadeInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
									<br>
									<span class="icon flaticon-planet-earth" style="font-size:60px; color:#eb0028"></span>
									<h3 span style="color:#eb0028; font-weight: 700; line-height:60px;">Our Vision</h3>
									<span class="designation" style="text-align: justify; font-size: 15px;">Our vision is to be South East Asia’s leading logistics solutions specialist in the avenues of innovational solutions and market share whilst expanding into the international market.</span>
									<!-- <ul class="social-icon-one">
										<li><a href="#"><span class="fa fa-twitter"></span></a></li>
										<li><a href="#"><span class="fa fa-facebook"></span></a></li>
										<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
									</ul> -->
								</div>
						<div class="tm-bx">
							<div class="tm-thmb">
								<div class="tm-inff wowallow fadeInDown" data-wow-delay="0ms" data-wow-duration="1500ms">
									<br>
									<span class="icon flaticon-target" style="font-size:63px; color:#eb0028;text-align: center; padding-left: 44%; padding-right: 44%;"></span>
									<h3 span style="color:#eb0028; font-weight: 700; line-height:60px; text-align: center;">Our Mission</h3>
									<span class="designation" style="text-align: justify; font-size: 15px;">Bumilink wants to be your transportation and logistics partner, assisting you in expanding your business by handling your freight, warehousing and distribution needs, wherever you require solutions. We want to be your resource partner for any logistics function that will make your business more efficient and cost effective. By being your logistics solution provider, we can show you dramatic savings in cost, expansion into new markets, and help you better meet customer demands. </span>
									<!-- <ul class="social-icon-one">
										<li><a href="#"><span class="fa fa-twitter"></span></a></li>
										<li><a href="#"><span class="fa fa-facebook"></span></a></li>
										<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
									</ul> -->
								</div>
							</div>
						</div>
					</div> 
				</div>
			</div><!-- Team Sec -->
		</div>
	</div>
</div>
    </section>

	<!-- Our technology Section -->
	<section class="price-section" style="background-image:url(images/background/home-tech-banner.png)">
		<div class="auto-container maxwidth-1400">

			<div class="sec-title-two centered light sec-title">
				<h2>Our <span>Technology</span></h2>
				<div class="separater"></div>
			</div>

			<div class="clearfix">

				<!-- Price Block -->
				<div class="price-block col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms"> 
						<div class="flaticon-cloud-computing-3" style="font-size:60px; padding:12%; color:#eb0028"></div>
						<div class="title-first">Logistics & Warehouse Management Software 
						</div>
						<!-- <div class="price-box">
							<div class="price">$50.0</div>
							<div class="months">/KM Distance</div>
						</div> -->
						<div class="price-list-first"> 
							<span class="tech-content para-content">BLink warehouse management system transforms warehouse operations into fully integrated logistics and fulfillment business. By enabling a company to take orders from multiple sources, process them in real time with mobile and automating the shipping and billing processes, organizational efficiency will improve, with positive effect on the bottom line.
								BLink is a comprehensive, easy to use software package for managing 3rd Party Warehouse operations. BLink features the ability to manage multiple warehouses, customers, products, rates, carriers, consignees, inbound receipts, bills of landing, and much more. BLink also produces reports of inventory, activity, and a wide variety of management and customer and product specific information.</span>
						</div>
						<!-- <div class="btn-box">
							<a href="#" class="theme-btn choose-btn">Choose Now</a>
						</div> -->
					</div>
				</div>
				
				<div class="price-block col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="flaticon-support" style="font-size:60px; padding:12%; color:#eb0028"></div>
						<div class="title">Ongoing Support</div> 
						<!-- <div class="price-box">
							<div class="price">$50.0</div>
							<div class="months">/KM Distance</div>
						</div> -->
						<div class="price-list"> 
							<span class="tech-content para-content">Having a successful software solution goes beyond installation and implementation. BLink offers full product support to help you troubleshoot your system, implement new features, and identify potential areas of product improvement. Whether you’re speaking to a support consultant over the phone or conversing via email, our staff is trained to look at the whole picture, not merely the question being asked. This helps ensure that your system stays optimized for your business and runs smoothly and continually.</span>
						</div>
						<!-- <div class="btn-box">
							<a href="#" class="theme-btn choose-btn">Choose Now</a>
						</div> -->
					</div>
				</div>

				<!-- Price Block -->
				<!-- <div class="price-block active col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box wowallow fadeInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="title">Platinum</div>
						<div class="price-box">
							<div class="price">$80.25</div>
							<div class="months">/KG Package</div>
						</div>
						<ul class="price-list">
							<li>Additional Single Truck</li>
							<li>200x Weeks</li>
							<li>Adminstrator Access</li>
							<li>Sponsored Promotion</li>
							<li>Premium Company Account</li>
							<li>ID Tracking Location</li>
						</ul>
						<div class="btn-box">
							<a href="#" class="theme-btn choose-btn">Choose Now</a>
						</div>
					</div>
				</div> -->

				<!-- Price Block -->
				<div class="price-block col-lg-4 col-md-6 col-sm-12">
					<div class="inner-box wowallow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
						<div class="flaticon-chat" style="font-size:60px; padding:12%; color:#eb0028"></div>
						<div class="title">By Phone and Online </div> 
						<!-- <div class="price-box">
							<div class="price">$75.0</div>
							<div class="months">/Mile Distance</div>
						</div> -->
						<div class="price-list-last"> 
							<span class="tech-content para-content">Our support organization is always available to you during normal business hours, but we also offer an optional support plan to assist after-hours and via a paging service. For those who prefer a more "self-service" option, our support website provides timely articles and product updates available only to current BLink support customers.</span>
						</div>
						<!-- <div class="btn-box">
							<a href="#" class="theme-btn choose-btn">Choose Now</a>
						</div> -->
					</div>
				</div> 
			</div>
		</div>
	</section>
	<!-- End Our technology Section -->

	<!-- Our way of business Section -->
	<section class="testimonial-section gap">
		<div class="auto-container">
			<div class="sec-title centered">
				<h3>Our <span>Way Of</span> Business</h3>
				<div class="separater"></div>
			</div>
			<div class="two-item-carousel owl-carousel owl-theme">

				<!-- Testimonial Block -->
				<div class="testimonial-block">
					<div class="inner-box">
						<div class="quote-icon flaticon-warehouse" style="color:#eb0028; font-size: 35px;"></div> 
						<h3 style="color:black; font-weight: 600; line-height:50px;"><span style="color:#eb0028; font-weight: 600; line-height:50px;">Operational</span> Excellence</h3>
						<div class="text-one">BumiLink’s operational standards have been honed in demanding environments as diverse as Formula 1,   Manufacturing, Grocery Consumer Supply Chains, Credit card Distribution, Industrial Logistics, Oil & Gas, Aviation & Aerospace, Publishing and Fulfillment. We apply our learning across all our sectors, and we are building and rolling out defined processes and best practice to ensure that every customer, project and site will benefit.</div>
						
					</div> 
				</div><!-- Testimonial Block -->

				<div class="testimonial-block">
					<div class="inner-box">
						<div class="quote-icon flaticon-hand-shake-1" style="color:#eb0028; font-size: 35px;"></div> 
						<h3 style="color:black; font-weight: 600; line-height:50px;"><span style="color:#eb0028; font-weight: 600; line-height:50px;">Customer</span> Relations</h3>
						<div class="text-two">Understanding a company's requirements is the first step in creating a tailor-made solution. We start by listening and challenging. We require all our staff to understand and develop their relationship with customers whether operational, administrative or commercial. Our management structure is aligned to supporting and adding to those relationships.

						<br><br>Throughout the life of a relationship from project phase, to start up, to renewal, we seek to perform, to improve, to anticipate and to innovate in line with our customers' needs.</span></div>
					</div> 
				</div><!-- Testimonial Block -->

				<div class="testimonial-block">
					<div class="inner-box">
						<div class="quote-icon flaticon-delivery-box" style="color:#eb0028; font-size: 35px;"></div> 
						<h3 style="color:black; font-weight: 600; line-height:50px;"><span style="color:#eb0028; font-weight: 600; line-height:50px;">Product</span> Leadership</h3>
						<div class="text-three">
							<ul style="list-style-type:square;">
								<li style="padding-bottom: 3%;">&#10004; BumiLink is a market leader in fully comprehensive integrated total logistics software. With our own in-house development we are able to customize at any level and at any point.</li>
								<li style="padding-bottom: 3%;">&#10004; BumiLink is the first to implement a fully integrated online grocery corporate & consumer supply chain software solution coupled with innovative forward logistics.
							</li>
							<li style="padding-bottom: 3%;">&#10004; BumiLink commitment to product leadership enables us to anticipate the future requirements of our customers, harness new technologies and respond to change.</li>
						</ul>
						</div>
					</div> 
				</div><!-- Testimonial Block -->

				<div class="testimonial-block">
					<div class="inner-box">
						<div class="quote-icon flaticon-verified" style="color:#eb0028; font-size: 35px;"></div> 
						<h3 style="color:black; font-weight: 600; line-height:50px;"><span style="color:#eb0028; font-weight: 600; line-height:50px;">Va</span>lue</h3>
						<div class="text-four">BumiLink builds all-round value for its customers by tailoring solutions to the precise needs of the customer and by our ability to optimize the performance and productivity of each solution. 
							<br><br>BumiLink offers long-term price differentiation by timely investment in relevant technologies and new product development and by investing directly in our customers' goals.</div>
						
					</div> 
				</div>
			</div>
		</div>
	</section>
	<!-- End Our way of business Section -->

	<section class="fullwidth-section">
		<div class="outer-container">
			<div class="clearfix">

				<!-- Left Column -->
				<div class="left-column" style="background-image:url(images/background/home-sender.png)">
					<div class="inner-column">
						<h3>Are You A Sender?</h3>
						<div class="text">We have diverse range of logistics services tailored to your needs in the market.</div>
						<a href="track.html" class="theme-btn btn-style-one">Check Packages</a>
					</div>
				</div>

				<!-- Right Column -->
				<div class="right-column" style="background-image:url(images/background/home-shipper.png)">
					<div class="inner-column">
						<h3>Are You A Shipper?</h3>
						<div class="text">We offer a broad range of value-added services that enable us to operate as a single source logistics provider.</div>
						<a href="contact.html" class="theme-btn btn-style-two">Contact us</a>
					</div>
				</div>

			</div>
		</div>
	</section>

	<!--Main Footer-->
    <footer class="main-footer style-two" style="background-image:url(images/background/7.png)">
    	<div class="auto-container">
        	<!--Widgets Section-->
            <div class="widgets-section">
            	<div class="row clearfix">

                    <!--Column-->
                    <div class="big-column col-lg-6 col-md-12 col-sm-12">
						<div class="row clearfix">

                        	<!--Footer Column-->
                            <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                                <div class="footer-widget logo-widget">
									<h2><span class="icon fa fa-thumbs-o-up" style="padding-left:6%;"></span>Useful Links</h2>

									<div class="row clearfix">
										<div class="column col-lg-12 col-md-6 col-sm-12">
											<ul class="footer-list">
												<li><a href="https://www.oanda.com/currency-converter/en/?from=EUR&to=USD&amount=1" target="_blank" rel="noopener noreferrer">Currencies Converter</a></li>
												<li><a href="#" target="_blank" rel="noopener noreferrer">Customs</a></li>
												<li><a href="https://www.dictionary.com/" target="_blank" rel="noopener noreferrer">Dictionary & Translator</a></li>
												<li><a href="http://www.helplinedatabase.com/embassy-database/" target="_blank" rel="noopener noreferrer">Embassy Worldwide</a></li>
												<li><a href="https://www.timeanddate.com/worldclock/" target="_blank" rel="noopener noreferrer">World Clock</a></li> 
												
											</ul>
										</div>
									</div>

								</div>
							</div>

							<!--Footer Column-->
                            <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                                <div class="footer-widget news-widget">
									<!-- <div class="footer-widget news-widget" style="margin-left:-20%; padding-right:15%;"></div> -->
									<h2 style="color: #eb002700;"><span class="icon fa fa-bullhorn" style="color:#eb002700"></span>Recent News</h2>

									<!--News Widget Block-->
									<div class="column col-lg-12 col-md-6 col-sm-12">
										<div class="news-widget-block">
										<ul class="footer-list"> 
											<!-- <ul class="footer-list" style="margin-right: 5%;">  -->
											<li><a href="http://www.earthcalendar.net/index.php" target="_blank" rel="noopener noreferrer">World Holiday</a></li>
											<li><a href="#" target="_blank" rel="noopener noreferrer">World International City Codes</a></li>
											<li><a href="https://www.google.com/maps/@1.5433728,103.7860864,13z" target="_blank" rel="noopener noreferrer">World Map</a></li>
											<li><a href="http://www.the-acr.com/codes/cntrycd.htm#a" target="_blank" rel="noopener noreferrer">Worldwide Phone Directory</a></li>
											
										</ul>
									</div>
									</div>
                                    <!-- <div class="news-widget-block">
                                        <div class="widget-inner">
                                            <div class="image">
                                                <img src="images/resource/news-image-1.jpg" alt="" />
                                            </div>
                                            <h3><a href="blog-detail.html">Lorem ipsum dolor sitam et, mandamus his ad</a></h3>
                                            <div class="post-date">Sept 26, 2018</div>
                                        </div>
                                    </div> -->

									<!--News Widget Block-->
                                    <!-- <div class="news-widget-block">
                                        <div class="widget-inner">
                                            <div class="image">
                                                <img src="images/resource/news-image-2.jpg" alt="" />
                                            </div>
                                            <h3><a href="blog-detail.html">Lorem ipsum dolor sitam et, mandamus his ad</a></h3>
                                            <div class="post-date">Oct 29, 2018</div>
                                        </div>
                                    </div> -->

									<!--News Widget Block-->
                                    <!-- <div class="news-widget-block">
                                        <div class="widget-inner">
                                            <div class="image">
                                                <img src="images/resource/news-image-3.jpg" alt="" />
                                            </div>
                                            <h3><a href="blog-detail.html">Lorem ipsum dolor sitam et, mandamus his ad</a></h3>
                                            <div class="post-date">Jan 26, 2020</div>
                                        </div>
                                    </div> -->

								</div>
							</div>

						</div>
					</div>

					<!--Column-->
                    <div class="big-column col-lg-6 col-md-12 col-sm-12">
						<div class="row clearfix"> 

							<!--Footer Column-->
                            <div class="footer-column col-lg-6 col-md-6 col-sm-12" >
                                <div class="footer-widget about-widget">
									<!-- <div class="footer-widget about-widget"style="margin-left:-7%; padding-right:5%"></div> -->
									<h2><span class="icon fa fa-user"></span>About Us</h2>
									<div class="column col-lg-12 col-md-6 col-sm-12">
										<ul class="footer-list"> 
											<!-- <ul class="footer-list" style="margin-left: -6%;">  -->
												<li><a href="domesticservices.html" target="_blank" rel="noopener noreferrer">Domestic Services</a></li>
												<li><a href="international.html" target="_blank" rel="noopener noreferrer">International</a></li>
												<li><a href="globallogistic.html" target="_blank" rel="noopener noreferrer">Global Logistics</a></li>
												<li><a href="custombrokerage.html" target="_blank" rel="noopener noreferrer">Customs Brokerage</a></li>
												<li><a href="cargoinsurance.html" target="_blank" rel="noopener noreferrer">Cargo Insurance</a></li>
										</ul>
									</div>
									<!-- <div class="text">Lorem ipsum dolor sit amet, conec tetur adipisicing elit, sed do eiusd tempor incididunt ut labore dolore magna aliqua tempor.</div>
									<div class="phone-number">0044 123 456</div>
									<div class="about-email">carga@example.com</div>-->
									
									<!--Social Box-->
									<!-- <ul class="social-icon-one">
										<li><a href="#"><span class="fa fa-twitter"></span></a></li>
										<li><a href="#"><span class="fa fa-facebook"></span></a></li>
										<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
										<li><a href="#"><span class="fa fa-google-plus"></span></a></li>
										<li><a href="#"><span class="fa fa-instagram"></span></a></li>
										<li><a href="#"><span class="fa fa-youtube"></span></a></li>
									</ul> -->
								</div>
							</div>

							<!--Footer Column-->
                            <div class="footer-column col-lg-6 col-md-6 col-sm-12">
                                <div class="footer-widget mail-widget" >
									<!-- <div class="footer-widget mail-widget" style="margin-left:-8%; padding-right:10%;"></div> -->
									<h2><span class="icon fa fa-envelope-o"></span>Contact Us</h2>
									<div class="text">Don't hesitate to contact us now!</div>
									<div class="phone-number"><a href="tel:+60351225355" style="color:#eb0028">03 51225355 </a></div>
									<div class="phone-number-fax"><a href="tel:+60351226355" style="color:#555555;">03 51226355 </a></div>
									<div class="about-email"><a href="mailto:cs@bumilink.com.my" style="color:#555555">cs@bumilink.com.my</a></div>
									<!--Social Box-->
									<ul class="social-icon-one">
										<li><a href="#"><span class="fa fa-twitter"></span></a></li>
										<li><a href="#"><span class="fa fa-facebook"></span></a></li>
										<li><a href="#"><span class="fa fa-linkedin"></span></a></li>
										<li><a href="#"><span class="fa fa-google-plus"></span></a></li>
										<li><a href="#"><span class="fa fa-instagram"></span></a></li>
										<li><a href="#"><span class="fa fa-youtube"></span></a></li>
									</ul>
									<!-- Email Form -->
									<!-- <div class="email-form">
										<form method="post" action="quote.html">
											<div class="form-group clearfix">
												<input type="email" name="email" value="" placeholder="Email address">
												<button type="submit" class="theme-btn submit-btn"><span class="icon fa fa-check"></span></button>
											</div>
										</form>
									</div> -->
									<!-- <div class="text wid-notice">(No Spam, We respect your privacy)</div> -->
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<!-- Footer Bottom -->
		<div class="footer-bottom">
			<div class="privacy">
				<a href="privacy.html">Privacy</a> | Terms Of Use | Resources</div>
			<div class="copyright">All Rights Reserved &copy; 2010 - 2023 / Bumilink Global Logistic Sdn Bhd | Powered by First Online</div>
		</div>

	</footer>

</div>
<!--End pagewrapper-->

<!--Scroll to top-->
<div class="scroll-to-top scroll-to-target" data-target="html"><span class="fa fa-arrow-up"></span></div>

<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/appear.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/script.js"></script>

</body>
</html>