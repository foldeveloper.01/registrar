@extends('admin.layouts.app')
    @section('styles')
    @endsection
        @section('content')

                           <!-- PAGE-HEADER -->
                           <div class="page-header">
                            <h1 class="page-title">Admin Login History</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li> -->
                                    <li class="breadcrumb-item" aria-current="page"><a href="javascript:void(0)">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Login History</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row row-sm">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Admin Login History List</h3>
                                    </div>
                                    <div class="card-body">
                                        <!-- <a href="{{url('admin/createcontactby')}}"> <button id="table2-new-row-button" class="btn btn-primary mb-4"> Add New</button></a> -->
                                        <div class="table-responsive">
                                            <table class="table border text-nowrap text-md-nowrap mb-0" id="responsive-datatable">
                                                <thead class="table-primary">
                                                    <tr>
                                                        <th class="wd-15p border-bottom-0">#</th>
                                                       <th class="wd-15p border-bottom-0">User Name</th>
                                                        <th class="wd-15p border-bottom-0">User Email</th>
                                                        <th class="wd-15p border-bottom-0">User Type</th>
                                                        <th class="wd-15p border-bottom-0">Last Login IP</th>
                                                        <th class="wd-15p border-bottom-0">Last Login At</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   
                                                     @if(@$list)
                                                    @foreach(@$list as $key=>$data)                                                    
                                                    <tr>                                                        
                                                        <td>{{$key+1}}</td>
                                                        <td>{{@$data->userDetail->firstname}}</td>
                                                         <td>{{@$data->userDetail->email}}</td>
                                                         <td>{{@$data->GetAdminType(@$data->userDetail->admin_type_id)}}</td>
                                                         <td>{{@$data->last_login_ip}}</td>
                                                        <td>{{ \Carbon\Carbon::parse(@$data->last_login_at)->format('d/m/Y h:i:s')}}</td>
                                                                                                        
                                                    </tr>
                                                    @endforeach
                                                    @endif   

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Row -->
             
        @endsection

    @section('scripts')

    <!-- Select2 js-->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>

    <!-- DATA TABLE JS-->
    <script src="{{asset('assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/dataTables.bootstrap5.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.bootstrap5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/jszip.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/responsive.bootstrap5.min.js')}}"></script>
    <script src="{{asset('assets/js/table-data.js')}}"></script>
    <!-- SWEET-ALERT JS -->
    <script src="{{asset('assets/plugins/sweet-alert/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/js/sweet-alert.js')}}"></script>

    <!-- INTERNAL Edit-Table JS -->
   <!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script>
    <script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
    <script type="text/javascript">
        $(document).ready(function() {
       /* var table = $('#responsive-datatable5').DataTable({
            dom: 'Bfrtip',
            select: true,
            lengthMenu: [
                  [10, 25, 50, -1],
                  ['10 rows', '25 rows', '50 rows', 'Show all rows']
              ],
             buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
             buttons: [
                  { extend: 'pdf', text: '<i class="fas fa-file-pdf fa-1x" aria-hidden="true">PDF</i>' },
                  { extend: 'csv', text: '<i class="fas fa-file-csv fa-1x">CSV</i>' },
                  { extend: 'excel', text: '<i class="fas fa-file-excel" aria-hidden="true">EXCEL</i>' },
                  { extend: 'copy', text: '<i class="fas fa-file-copy" aria-hidden="true">COPY</i>' },
                  { extend: 'print', text: '<i class="fas fa-file-print" aria-hidden="true">PRINT</i>' },
                  'pageLength'
              ],
        });
        table.buttons().container()
              .appendTo('#datatable_wrapper .col-md-6:eq(0)');*/
        });
    </script>
    @endsection
