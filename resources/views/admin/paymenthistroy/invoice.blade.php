@extends('admin.layouts.app')
    @section('styles')
    <style type="text/css">@media print {
  #printPageButton,#printbackButton {
    display: none;
  }
}</style>
    @endsection
        @section('content')
                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Payment Invoice</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">Payment</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Invoice</li> -->
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <a class="header-brand" href="{{url('index')}}">
                                                   <img class="header-brand-img logo" src="{{asset('storage/images/settings/'.@$setting->logo)}}"/>
                                                    <!-- <img src="{{asset('assets/images/brand/logo.png')}}" class="header-brand-img logo" alt="Sash logo"> -->
                                                </a>
                                                <div>
                                                    <address class="pt-3">
                                                        {{Auth()->guard('admin')->user()->address}}<br>
                                                        {{Auth()->guard('admin')->user()->email}}
                                                    </address>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 text-end border-bottom border-lg-0">
                                                <h3>#INV-{{@$data->receipt_number}}</h3>
                                                <h5>Date Issued: {{@$data->created_at}}</h5>
                                                <h5>Due Date: {{@$data->expiry_at}}</h5>
                                            </div>
                                        </div>
                                        <div class="row pt-5">
                                            <div class="col-lg-6">
                                                <p class="h3">Invoice To:</p>
                                                <p class="fs-18 fw-semibold mb-0">{{@$data->full_name}}</p>
                                                <address>
                                                        {{@$data->address}}, {{@$data->city}}, {{@$data->state}}<br>
                                                        {{@$data->country}}<br>
                                                        {{@$data->postal_code}}<br>
                                                        {{@$data->email}}
                                                    </address>
                                            </div>
                                           <!--  <div class="col-lg-6 text-end">
                                                <p class="h4 fw-semibold">Payment Details:</p>
                                                <p class="mb-1">Total Due: $5,89,789</p>
                                                <p class="mb-1">Bank Name: Union Bank 0456</p>
                                                <p class="mb-1">IBAN: 543218769</p>
                                                <p>Country: USA</p>
                                            </div> -->
                                        </div>
                                        <div class="table-responsive push">
                                            <table class="table table-bordered table-hover mb-0 text-nowrap">
                                                <tbody>
                                                    <tr class=" ">
                                                        <th class="text-center">#</th>
                                                        <th class="text-center">Domain</th>
                                                        <th class="text-center">OrderId</th>
                                                        <th class="text-center">Receipt Number</th>
                                                        <th class="text-center">Payment Mode</th>
                                                        <th class="text-center">Payment Status</th>
                                                        <th class="text-center">Amount</th>
                                                    </tr>
                                                    
                                                                                                     
                                                    
                                                        <tr>
                                                             <td class="text-center">1</td>
                                                             <td class="text-center">{{@$data->getPayinvoiceData->domain_name}}</td>
                                                             <td class="text-center">{{@$data->order_id}}</td>
                                                             <td class="text-center">{{@$data->receipt_number}}</td>
                                                             <td class="text-center">{{@$data->getPaymentMode->name}}</td>
                                                             <td class="text-center">{{@$data->payment_status}}</td>
                                                             <td class="text-end">MYR {{@$data->amount}}</td>                                                 
                                                        </tr>


                                                    <tr>
                                                        <td colspan="6" class="fw-bold text-uppercase text-end">Total</td>
                                                        <td class="fw-bold text-end h4">MYR {{@$data->amount}}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="card-footer text-end"> 
                                        <a href="{{url('admin/paymenthistroy')}}"><button id="printbackButton" class="btn btn-primary">Back</button></a>
                                        <button type="button" id="printPageButton" class="btn btn-danger mb-1" onclick="javascript:window.print();"><i class="si si-printer"></i> Print Invoice</button>
                                    </div>
                                </div>
                            </div>
                            <!-- COL-END -->
                        </div>
                        <!-- ROW-1 CLOSED -->

        @endsection

    @section('scripts')



    @endsection
