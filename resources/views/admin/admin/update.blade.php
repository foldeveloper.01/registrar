@extends('admin.layouts.app')
@section('styles')
@endsection
@section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
   <h1 class="page-title">Update Profile</h1>
   <div>
      <ol class="breadcrumb">
         <li class="breadcrumb-item"><a href="{{url('admin/admins')}}">Admin</a></li>
         <li class="breadcrumb-item active" aria-current="page">Update Profile</li>
      </ol>
   </div>
</div>
<!-- PAGE-HEADER END -->


<!-- ROW-1 OPEN -->
                        <div class="row">
                            
                            <div class="col-xl-4">
                                <div class="card">
                                     <form id="update_user_pass" action="{{url('admin/admins/update/'.Request::segment(4))}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="action_page" value="password">
                                    <div class="card-header">
                                        <div class="card-title">Update Password</div>
                                    </div>
                                    <div class="card-body">
                                        <div class="text-center chat-image mb-5">
                                            <div class="avatar avatar-xxl chat-profile mb-3 brround">
                                                <img alt="avatar" src="{{asset('storage/images/user/user_image.png')}}" class="brround">
                                            </div>
                                            <div class="main-chat-msg-name">                                                
                                                    <h5 class="mb-1 text-dark fw-semibold">{{@$getuserData->firstname}}</h5>
                                                <p class="text-muted mt-0 mb-0 pt-0 fs-13">{{@$getuserData->GetAdminType(@$getuserData->admin_type_id)}}</p>
                                            </div>
                                        </div>
                                        @if(Auth()->guard('admin')->user()->admin_type_id!='1')
                                        <div class="form-group">
                                            <label class="form-label">Current Password</label>
                                            <div class="wrap-input100 validate-input input-group" id="Password-toggle">
                                                <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                    <i class="zmdi zmdi-eye text-muted" aria-hidden="true"></i>
                                                </a>
                                                <input class="input100 form-control" type="password" placeholder="Current Password" name="current_password" id="current_password" required>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="form-group">
                                            <label class="form-label">New Password</label>
                                            <div class="wrap-input100 validate-input input-group" id="Password-toggle1">
                                                <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                    <i class="zmdi zmdi-eye text-muted" aria-hidden="true"></i>
                                                </a>
                                                <input class="input100 form-control" type="password" placeholder="New Password" name="new_password" id="new_password" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="form-label">Confirm Password</label>
                                            <div class="wrap-input100 validate-input input-group" id="Password-toggle2">
                                                <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                                    <i class="zmdi zmdi-eye text-muted" aria-hidden="true"></i>
                                                </a>
                                                <input class="input100 form-control" type="password" placeholder="Confirm Password" name="confirm_password" id="confirm_password" required>
                                            </div>
                                        </div>
                                        <span id="divCheckPasswordMatch"></span>
                                    </div>
                                    <div class="card-footer text-end">
                                        <button type="submit" id="password_update_button" class="btn btn-success my-1">Update</button>
                                        <a href="{{url('admin/admins')}}" class="btn btn-danger my-1">Cancel</a>
                                    </div>
                                </form>
                                </div>

                                <div class="card panel-theme">
                                    <div class="card-header">
                                        <div class="float-start">
                                            <h3 class="card-title">Contact</h3>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="card-body no-padding">
                                        <ul class="list-group no-margin">
                                            <li class="list-group-item d-flex ps-3">
                                                <div class="social social-profile-buttons me-2">
                                                    <a class="social-icon text-primary" href=""><i class="fe fe-mail"></i></a>
                                                </div>
                                                <span class="my-auto">{{@$getuserData->email}}</span>
                                            </li>
                                            <li class="list-group-item d-flex ps-3">
                                                <div class="social social-profile-buttons me-2">
                                                    <a class="social-icon text-primary" href=""><i class="fe fe-globe"></i></a>
                                                </div> 
                                                <span class="my-auto">{{@$getuserData->organization}}</span>
                                            </li>
                                            <li class="list-group-item d-flex ps-3">
                                                <div class="social social-profile-buttons me-2">
                                                    <a class="social-icon text-primary" href=""><i class="fe fe-phone"></i></a>
                                                </div>
                                                <span class="my-auto">{{@$getuserData->phone_number}}</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-8">
                        <form id="update_user" action="{{url('admin/admins/update/'.Request::segment(4))}}" method="post" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="action_page" value="profile">
                            <div class="col-xl-8">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Update Profile</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname">Name</label>
                                                    <input type="text" value="{{@$getuserData->firstname}}"  name="firstname" class="form-control" id="firstname" placeholder="Name">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                   <label for="exampleInputEmail1">Email address</label>
                                                      <input type="email" name="email" value="{{@$getuserData->email}}" class="form-control" id="email" placeholder="Email address">
                                                      @if($errors->has('email'))
                                                      <div class="error">{{ $errors->first('email') }}</div>
                                                      @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputnumber">Contact Number</label>
                                                     <input type="number" name="number" value="{{@$getuserData->phone_number}}" class="form-control" id="number" placeholder="Contact number" maxlength="12" pattern="^(\+?6?01)[0-46-9]-*[0-9]{7,8}$" >
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                   <label for="exampleInputnumber">Organization Name</label>
                                                   <input type="text" name="organization_name" value="{{@$getuserData->organization}}" class="form-control" id="organization_name" placeholder="Organization Name">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label class="form-label">Organization type</label>
                                                      <select name="organization_type" class="form-control select2-show-search form-select">
                                                         <option value="">Please select one</option>
                                                         @if(@$domaintype)
                                                         @foreach(@$domaintype as $key=>$value)
                                                         <option value="{{@$value->id}}" @if(@$value->id==@$getuserData->organization_type) selected="selected" @endif>{{@$value->domain_type}}</option>
                                                         @endforeach
                                                         @endif                                                            
                                                      </select>
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                  <label class="form-label">User Type</label>
                                                  <select name="usertype" class="form-control select2-show-search form-select">
                                                     <option value="">Please select one</option>
                                                     @if(@$admintypes)
                                                     @foreach(@$admintypes as $key=>$value)
                                                     <option value="{{@$value->id}}" @if(@$value->id==@$getuserData->admin_type_id) selected="selected" @endif>{{@$value->type}}</option>
                                                     @endforeach
                                                     @endif                                                            
                                                  </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label class="form-label">Address</label>
                                            <textarea id="address" name="address" class="form-control mb-4" placeholder="Address" rows="2">{{@$getuserData->address}}</textarea>
                                        </div>
                                        <div class="form-group">
                                          <label class="form-label">Extra Details</label>
                                          <textarea id="extra_details" name="extra_details" class="form-control mb-4" placeholder="Extra Details" rows="2">{{@$getuserData->additional_details}}</textarea>
                                       </div>
                                       <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label class="form-label">Post Code</label>
                                                    <input type="text" value="{{@$getuserData->postal_code}}"  name="postcode" class="form-control" id="postcode" placeholder="Post code">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                   <label class="form-label">City</label>
                                                   <input type="text" value="{{@$getuserData->city}}"  name="city" class="form-control" id="city" placeholder="City">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label class="form-label">State</label>
                                                    <input type="text" value="{{@$getuserData->state}}"  name="state" class="form-control" id="state" placeholder="State">
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                   <label class="form-label">Country</label>
                                                      <select name="country" class="form-control select2-show-search form-select">
                                                         <option value="">Please select one</option>
                                                         @if(@$country)
                                                         @foreach(@$country as $key=>$value)
                                                         <option value="{{@$value->id}}" @if(@$value->id==@$getuserData->country) selected="selected" @endif>{{@$value->nicename}}</option>
                                                         @endforeach
                                                         @endif                                                            
                                                      </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label class="form-label">Status</label>
                                                      <select name="status" class="form-control select2-show-search form-select">
                                                      <option value="0" @if(@$getuserData->status==0) selected="selected" @endif>Active</option>
                                                      <option value="1" @if(@$getuserData->status==1) selected="selected" @endif>Suspend</option>
                                                      </select>
                                                </div>
                                            </div>                                            
                                        </div>

                                       
                                    </div>
                                    <div class="card-footer text-end">
                                        <button type="submit" class="btn btn-success my-1">Update</button>
                                        <a href="{{url('admin/admins')}}" class="btn btn-danger my-1">Cancel</a>
                                    </div>
                                </div>                                
                            </div>
                        </form>
                        </div>


                    </div>
                        <!-- ROW-1 CLOSED -->


@endsection
@section('scripts')
<!-- INTERNAL SELECT2 JS -->
<!-- SELECT2 JS -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>
<script type="text/javascript">
   $(document).ready(function () {
      $("#new_password, #confirm_password").keyup(checkPasswordMatch);

        //check password
        $('#current_password').keyup(function(){
             // Department id
             var password = $(this).val();
             var id = "{{@$getuserData->id}}";
             // AJAX request 
             $.ajax({
                 url: '/admin/admins/checkCurrentPassword/'+id+'/'+password,
                 type: 'get',
                 dataType: 'json',
                 success: function(response){
                     var len = 0;
                     if(response['data'] != null){
                        if(response['data']==true){
                            $("#new_password").prop( "disabled", false );
                            $("#confirm_password").prop( "disabled", false );
                        }else{
                            $("#new_password").prop( "disabled", true );
                            $("#confirm_password").prop( "disabled", true );
                        }
                       // $("#exit-button").css("display", "none");
                    }                              
                }
            });
        });
   });
   
   $("body").on('click', '.toggle-password', function() {
     $(this).toggleClass("fa-eye fa-eye-slash");
     var input = $("#password");
     if (input.attr("type") === "password") {
       input.attr("type", "text");
   } else {
       input.attr("type", "password");
   }
   });
   $("body").on('click', '.toggle-password1', function() {
     $(this).toggleClass("fa-eye fa-eye-slash");
     var input = $("#password_confirmation");
     if (input.attr("type") === "password") {
       input.attr("type", "text");
   } else {
       input.attr("type", "password");
   }
   });
   function send_email(event, id, email) {
              event.preventDefault();
                swal({
                    title: "Password will be sent to "+email+". Is the email correct?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: '#DD6B55',
                    confirmButtonText: 'Send',
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function(isConfirm) {
                if (isConfirm) {
                        $("#send_email"+id).submit();                       
                } else {
                    swal("Cancel", "You Canceled this one", "error");
                }
        });
    }
   
   function checkPasswordMatch() {
       var password = $("#new_password").val();
       var oldpassword = $("#current_password").val();
       if(password == $(this).val()){
           if(oldpassword == $(this).val()){
              $('#password_update_button').prop('disabled', true);
              $("#divCheckPasswordMatch").html('Current password and new password cannot be same!').css('color', 'red'); 
           }else{
             $('#password_update_button').prop('disabled', false);
             $("#divCheckPasswordMatch").html('');
           }
       }else{
           $("#divCheckPasswordMatch").html('Passwords do not match!').css('color', 'red');
           $('#password_update_button').prop('disabled', true);
       }
   }        
           // just for the demos, avoids form submit
   jQuery.validator.setDefaults({
     debug: true,
     success: "valid"
   });
   $( "#update_user" ).validate({
       submitHandler : function(form) {
           form.submit();
       },
       rules: {
           firstname: {
             required: true,
             minlength: 3
         },
         email: {
             required: true,
             email:true,
         },
         usertype: {
             required: true
         }
     },
     messages: {        
       firstname: {
           required: "Please enter name",
       },
       email: {
           required: "Please enter email",
       },
       usertype: {
           required: "Please select usertype",
       }
   },
   });

   $( "#update_user_pass" ).validate({
       submitHandler : function(form) {
           form.submit();
       },
       rules: {
           current_password: {
             required: true
         },
         new_password: {
             required: true,
             minlength: 8
         },
         confirm_password: {
             required: true,
             minlength: 8
         }
     }     
   });
</script>
@endsection