@extends('admin.layouts.app')

    @section('styles')

    @endsection

        @section('content')

                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Dashboard</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 -->

                      


                    

                    <!-- ROW-1 OPEN -->
                        <div class="row">
                            <div class="row">
                                <form id="search_domain_form" class="" action="{{url('admin')}}" method="post">
                                  @csrf
                                    <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 ">                          
                                        <div class="card-body">
                                            <div class="input-group">
                                            <input type="text" id="domain_search" name="domain_search" class="form-control border-end-1" placeholder="Search ..." style="margin-left: -37px" value="@if(Request::isMethod('post')==1){{session('domain_search')}}@endif" required>
                                            <button class="btn btn-default py-3 px-3" style="background-color: #ed1f26;color: white;">Search Domain</button>
                                            </div>
                                        </div>                            
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-6 col-xl-3 col-md-12 col-lg-6">
                                <form id="domain_purchase_form" action="{{url('admin/domains/checkout')}}" method="post">
                                    @csrf
                                   <div class="panel price panel-color">
                                    <div class="pb-4 ps-5 border-bottom">
                                        <h3 class="pb-2">{{session('domain_search')}}.com</h3>
                                        <span> Sample content. </span>
                                    </div>
                                    <div class="panel-body p-0 ps-5">
                                        <p class="lead py-0 text-primary"><strong>MYR 15.00 </strong>/ Year</p>
                                        <!-- <p><i class="fe fe-plus me-1"></i>MYR 1.9 per Listing</p> -->
                                    </div>
                                    <div class="panel-footer text-center px-5 border-0 pt-0">
                                        <a class="btn btn-block btn-primary btn-pill" href="{{url('admin/domains/checkout')}}">Purchase Now!</a>
                                    </div>
                                    <ul class="list-group list-group-flush pb-5">
                                        <li class="list-group-item border-0"><i class="mdi mdi-circle-outline text-primary p-2 fs-12"></i> Sample Content </li>
                                    </ul>
                                  </div>
                                </form>
                            </div>
                            <!-- COL-END -->
                            <div class="col-sm-6 col-xl-3 col-md-12 col-lg-6">
                                <form id="domain_purchase_form" action="{{url('admin/domains/checkout')}}" method="post">
                                    @csrf
                                      <div class="panel price panel-color">
                                        <div class="pb-4 ps-5 border-bottom">
                                            <h3 class="pb-2">{{session('domain_search')}}.asia</h3>
                                            <span>Sample content</span>
                                        </div>
                                        <div class="panel-body p-0 ps-5">
                                            <p class="lead py-0 text-secondary"><strong>MYR 25.00 </strong>/ Year</p>
                                            <!-- <p><i class="fe fe-plus me-1"></i>MYR 1.9 per Listing</p> -->
                                        </div>
                                        <div class="panel-footer text-center px-5 border-0 pt-0">
                                            <a class="btn btn-block btn-secondary btn-pill" href="{{url('admin/domains/checkout')}}">Purchase Now!</a>
                                        </div>
                                        <ul class="list-group list-group-flush pb-5">
                                            <li class="list-group-item border-0"><i class="mdi mdi-circle-outline text-secondary p-2 fs-12"></i> Sample Content  </li>
                                        </ul>
                                     </div>
                                </form>
                            </div>
                            <!-- COL-END -->
                            <!-- <div class="col-sm-6 col-xl-3 col-md-12 col-lg-6">
                                <div class="panel price panel-color bg-success-transparent">
                                    <div class="pb-4 ps-5 border-bottom">
                                        <h3 class="pb-2">Business <span class="badge bg-success ms-5 mb-1 mt-1 fs-12 text-end">Recommended</span></h3>
                                        <span>Try it Free, Get access to all Features for 60 days. No plan credit card required. </span>
                                    </div>
                                    <div class="panel-body p-0 ps-5">
                                        <p class="lead py-0 text-success"><strong>$99 </strong>/ month</p>
                                        <p class="mb-0 pb-4"><i class="fe fe-plus me-1"></i>$1.9 per Listing</p>
                                    </div>
                                    <div class="panel-footer text-center px-5 border-0 pt-0">
                                        <a class="btn btn-block btn-success btn-pill" href="javascript:void(0)">Purchase Now!</a>
                                    </div>
                                    <ul class="list-group list-group-flush pb-5">
                                        <li class="list-group-item border-0"><i class="mdi mdi-circle-outline text-success p-2 fs-12"></i><strong> 5 Free</strong> Domain Name</li>
                                    </ul>
                                </div>
                            </div> -->
                            <!-- COL-END -->
                            <!-- <div class="col-sm-6 col-xl-3 col-md-12 col-lg-6">
                                <div class="panel price panel-color">
                                    <div class="pb-4 ps-5 border-bottom">
                                        <h3 class="pb-2">Corporate</h3>
                                        <span>Try it Free, Get access to all Features for 60 days. No plan credit card required. </span>
                                    </div>
                                    <div class="panel-body p-0 ps-5">
                                        <p class="lead py-0 text-danger"><strong>$35 </strong>/ month</p>
                                        <p><i class="fe fe-plus me-1"></i>$1.9 per Listing</p>
                                    </div>
                                    <div class="panel-footer text-center px-5 border-0 pt-0">
                                        <a class="btn btn-block btn-danger btn-pill" href="javascript:void(0)">Purchase Now!</a>
                                    </div>
                                    <ul class="list-group list-group-flush pb-5">
                                        <li class="list-group-item border-0"><i class="mdi mdi-circle-outline text-danger p-2 fs-12"></i><strong> 4 Free</strong> Domain Name</li>
                                    </ul>
                                </div>
                            </div> -->
                            <!-- COL-END -->
                        </div>
                        <!-- ROW-1 CLOSED -->
                        <!-- ROW-1 END -->

                        <!-- ROW-2 -->
                        <div class="row">
                            <!-- COL END -->
                            <!-- COL END -->
                        </div>
                        <!-- ROW-2 END -->                      
                        </div>
                        <!-- ROW-3 END -->
                        <!-- ROW-4 -->                        
                        <!-- ROW-4 END -->

        @endsection

    @section('scripts')

    <!-- SPARKLINE JS-->
    <script src="{{asset('assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>

    <!-- CHART-CIRCLE JS-->
    <script src="{{asset('assets/plugins/circle-progress/circle-progress.min.js')}}"></script>

    <!-- INTERNAL SELECT2 JS -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>

    <!-- PIETY CHART JS-->
    <script src="{{asset('assets/plugins/peitychart/jquery.peity.min.js')}}"></script>
    <script src="{{asset('assets/plugins/peitychart/peitychart.init.js')}}"></script>

    <!-- INTERNAL CHARTJS CHART JS-->
    <script src="{{asset('assets/plugins/chart/Chart.bundle.js')}}"></script>
    <script src="{{asset('assets/plugins/chart/rounded-barchart.js')}}"></script>
    <script src="{{asset('assets/plugins/chart/utils.js')}}"></script>

    <!-- INTERNAL SELECT2 JS -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>

    <!-- INTERNAL Data tables js-->
    <script src="{{asset('assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/dataTables.bootstrap5.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/js/table-data.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/responsive.bootstrap5.min.js')}}"></script>
        <script src="{{asset('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.bootstrap5.min.js')}}"></script>
       <script src="{{asset('assets/plugins/datatable/js/jszip.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>

    <!-- INTERNAL APEXCHART JS -->
    <script src="{{asset('assets/js/apexcharts.js')}}"></script>
    <script src="{{asset('assets/plugins/apexchart/irregular-data-series.js')}}"></script>

    <!-- C3 CHART JS -->
    <script src="{{asset('assets/plugins/charts-c3/d3.v5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/charts-c3/c3-chart.js')}}"></script>

    <!-- CHART-DONUT JS -->
    <script src="{{asset('assets/js/charts.js')}}"></script>

    <!-- INTERNAL Flot JS -->
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.fillbetween.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/chart.flot.sampledata.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/dashboard.sampledata.js')}}"></script>

    <!-- INTERNAL Vector js -->
    <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

    <!-- INTERNAL INDEX JS -->
    <script src="{{asset('assets/js/index.js')}}"></script>
    <script src="{{asset('assets/js/index1.js')}}"></script>
    <script type="text/javascript">
        $("#responsive-datatable1").DataTable({language:{searchPlaceholder:"Search...",scrollX:"100%",sSearch:""}});
    </script>

    @endsection
