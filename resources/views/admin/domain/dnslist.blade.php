@extends('admin.layouts.app')
    @section('styles')
    @endsection
        @section('content')

                           <!-- PAGE-HEADER -->
                           <div class="page-header">
                            <h1 class="page-title">DNS</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li> -->
                                    <li class="breadcrumb-item" aria-current="page"><a href="javascript:void(0)">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">DNS</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row row-sm">
                            <div class="col-md-12 col-xl-10">
                                <div class="card">                                    
                                    <div class="card-body">                                      
                                            <div class="">
                                                <div class="form-group">
                                                    <label for="exampleInputEmail1" class="form-label">Manage Your Domain</label>
                                                    <select id="domain" name="domain" class="form-control select2-show-search form-select" data-placeholder="Choose one">
                                                            @if(@$list)
                                                              @foreach(@$list as $key=>$value)
                                                              <option label="Choose one"></option>
                                                              <option value="{{@$value->id}}" @if(Session::get('selected_id')==@$value->id) selected @endif>{{@$value->domain_name}}</option>
                                                              @endforeach
                                                            @endif                                                            
                                                    </select>
                                                </div>
                                                
                                                <div class="card">                                
                                                <div class="card-body">
                                                    <div class="panel panel-primary">                                       
                                                        <div class="panel-body tabs-menu-body">
                                                            <div class="tab-content">
                                                                <div class="tab-pane active " id="tab14">                                                    
                                                                    <div class="row">
                                                                            <div class="col-md-12 col-xl-6">
                                                                            <div class="card">
                                                                                <div class="card-header" style="color: white;background-color: #5a5a5a;padding: 0.2rem 0.5rem;">
                                                                                    <div class="card-title">NAMESERVER 1</div>
                                                                                    <button id="exit-button" class="btn btn-primary" style="margin-right: 0;margin-left:auto;display:none;">Edit</button>
                                                                                </div>
                                                                                <form class="form-horizontal" action="{{url('admin/dns')}}" method="post">
                                                                                @csrf
                                                                                <input type="hidden" id="action_id"  name="action_id" value="">
                                                                                <div class="card-body nameserver-hostname-border">
                                                                                    <!-- content -->
                                                                                    <div class="content">
                                                                                        <div class="card-body">                
                                                                                            <div class=" row mb-4">
                                                                                                <label for="inputName" class="col-md-3 form-label">Hostname</label>
                                                                                                <div class="col-md-9">
                                                                                                    <input type="text" class="form-control" placeholder="Host name" id="nameserver1" name="nameserver1" value="" readonly>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="mb-0 mt-4 row justify-content-end">
                                                                                                <div class="col-md-9">
                                                                                                    <button id="update-button" class="btn btn-success my-1" style="display:none">update</button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-12 col-xl-6">   
                                                                            <div class="card">
                                                                                <div class="card-header" style="color: white;background-color: #5a5a5a;padding: 0.2rem 0.5rem;">
                                                                                    <div class="card-title">NAMESERVER 2</div>
                                                                                    <button id="exit-button1" class="btn btn-primary" style="margin-right: 0;margin-left:auto;display:none;">Edit</button>
                                                                                </div>
                                                                                 <form class="form-horizontal" action="{{url('admin/dns')}}" method="post">
                                                                                @csrf
                                                                                <input type="hidden" id="action_id1"  name="action_id1" value="">
                                                                                <div class="card-body nameserver-hostname-border">
                                                                                    <!-- content -->
                                                                                    <div class="content">
                                                                                        <div class="card-body">
                                                                                            <div class=" row mb-4">
                                                                                                <label for="inputName" class="col-md-3 form-label">Hostname</label>
                                                                                                <div class="col-md-9">
                                                                                                    <input type="text" class="form-control" placeholder="Host name" id="nameserver2" name="nameserver2" value="" readonly>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="mb-0 mt-4 row justify-content-end">
                                                                                                <div class="col-md-9">
                                                                                                    <button id="update-button1" class="btn btn-success my-1" style="display:none">update</button>
                                                                                                </div>
                                                                                            </div>
                                                                                    </div>
                                                                                </div>
                                                                                </div>
                                                                                
                                                                                        </form>
                                                                            </div>
                                                                        </div>                                                                      

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Row -->
             
        @endsection

    @section('scripts')
    <!-- DATA TABLE JS-->
    <script src="{{asset('assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/dataTables.bootstrap5.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.bootstrap5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/jszip.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/responsive.bootstrap5.min.js')}}"></script>
    <script src="{{asset('assets/js/table-data.js')}}"></script>
    <!-- SWEET-ALERT JS -->
    <script src="{{asset('assets/plugins/sweet-alert/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/js/sweet-alert.js')}}"></script>
     <script src="{{asset('assets/plugins/jquery-steps/jquery.steps.min.js')}}"></script>
    <script src="{{asset('assets/plugins/accordion-Wizard-Form/jquery.accordion-wizard.min.js')}}"></script>
    <script src="{{asset('assets/js/form-wizard.js')}}"></script>
    <!-- SELECT2 JS -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>

    <script type="text/javascript">
      $("document").ready(function () {
        
            //get domain data
            $(document).ready(function(){
                    // Department Change
                    $('#domain').change(function(){
                         // Department id
                         var id = $(this).val();
                         //alert(id);
                         // Empty the dropdown
                         $('#nameserver1').val("");
                         $('#nameserver2').val("");
                         // AJAX request 
                         $.ajax({
                             url: '/admin/dns/getdomainservername/'+id,
                             type: 'get',
                             dataType: 'json',
                             success: function(response){
                                 var len = 0;
                                 if(response['data'] != null){
                                    //console.log(response['data'].hostname1);
                                    $('#nameserver1').val(response['data'].hostname1);
                                    $('#action_id').val(response['data'].id);
                                    $('#nameserver2').val(response['data'].hostname2);
                                    $('#action_id1').val(response['data'].id);
                                    $("#exit-button").css("display", "block");
                                    $("#exit-button1").css("display", "block");
                                }else{
                                    $("#exit-button").css("display", "none");
                                    $("#exit-button1").css("display", "none");
                                }                                 
                            }
                        });
                    });
                    var domain_trigger = "{{Session::get('selected_id')}}";
                    if(domain_trigger!=''){
                        $('#domain').trigger('change');
                    }
                });
                //get domain data  btn btn-danger my-1
        $("#exit-button").click(function(){
            if($(this).html()=="Edit"){
              $("#exit-button1").prop("disabled", true);
              $("#update-button").css("display", "block");
              $("#exit-button").html("Cancel").addClass('btn-danger').removeClass('btn-primary');
              $("#nameserver1").prop("readonly", false);
            }else{
              $("#exit-button1").prop("disabled", false);
              $("#update-button").css("display", "none");
              $("#exit-button").html("Edit").addClass('btn-primary').removeClass('btn-danger');
              $("#nameserver1").prop("readonly", true);
            }           
        });
        $("#exit-button1").click(function(){
            if($(this).html()=="Edit"){
              $("#exit-button").prop("disabled", true);
              $("#update-button1").css("display", "block");
              $("#exit-button1").html("Cancel").addClass('btn-danger').removeClass('btn-primary');
              $("#nameserver2").prop("readonly", false);
            }else{
              $("#exit-button").prop("disabled", false);
              $("#update-button1").css("display", "none");
              $("#exit-button1").html("Edit").addClass('btn-primary').removeClass('btn-danger');
              $("#nameserver2").prop("readonly", true);
            }           
        });
       
        //datetimepicker
      $("#responsive-datatable4").dataTable({        
         language:{searchPlaceholder:"Search...",scrollX:"100%",sSearch:""},
         "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',       
      });
      var table = $('#responsive-datatable4').DataTable();
      $("#filterTable_filter.dataTables_filter").append($("#categoryFilter"));
      var categoryIndex = 0;
      $("#responsive-datatable4 th").each(function (i) {
        if ($($(this)).html() == "Status") {
          categoryIndex = i; return false;
        }
      });
      $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
          var selectedItem = $('#categoryFilter').val()
          var category = data[categoryIndex];
          if (selectedItem === "" || category.includes(selectedItem)) {
            return true;
          }
          return false;
        }
      );
      $("#categoryFilter").change(function (e) {
        table.draw();
      });
      table.draw();
    });

    </script>
    @endsection
