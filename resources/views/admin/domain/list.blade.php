@extends('admin.layouts.app')
    @section('styles')
    <style type="text/css">
  select.form-control{
    display: inline;
    width: 200px;
    /* margin-left: 25px; */
    float: right;
    left: 1px;
    margin-left: 10px;
    margin-top: 2px;
  }
</style>
    @endsection
        @section('content')

                           <!-- PAGE-HEADER -->
                           <div class="page-header">
                            <h1 class="page-title">Domain</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li> -->
                                    <li class="breadcrumb-item" aria-current="page"><a href="javascript:void(0)">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Domain</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row row-sm">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Domain List</h3>
                                    </div>
                                    <div class="card-body">
                                        <!-- <a href="{{url('admin/createcontactby')}}"> <button id="table2-new-row-button" class="btn btn-primary mb-4"> Add New</button></a> -->
                                        <div class="table-responsive">                                            
                                            <table class="table border text-nowrap text-md-nowrap mb-0" id="responsive-datatable4">
                                                <div class="category-filter">
                                                  <select id="categoryFilter" class="form-control">
                                                    <option value="" selected>Status: All</option>
                                                    <option value="Active">Status: Active</option>
                                                    <option value="Expired">Status: Expired</option>
                                                  </select>
                                                </div>
                                                <thead class="table-primary">
                                                    <tr>
                                                        <th class="wd-15p border-bottom-0">#</th>
                                                       <th class="wd-15p border-bottom-0">Domain</th>
                                                        <th class="wd-15p border-bottom-0">Registrant Name</th>
                                                        <th class="wd-15p border-bottom-0">Created At</th>
                                                        <th class="wd-15p border-bottom-0">Expiry Date</th>
                                                        <th class="wd-15p border-bottom-0">Status</th>
                                                        <th class="wd-15p border-bottom-0">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                   
                                                     @if(@$list)
                                                    @foreach(@$list as $key=>$data)                                                    
                                                    <tr>                                                        
                                                        <td>{{$key+1}}</td>
                                                        <td>{{@$data->domain_name}}</td>
                                                         <td>{{@$data->registration_name}}</td>
                                                         <td>{{ \Carbon\Carbon::parse(@$data->created_at)->format('d/m/Y h:i')}}</td>
                                                         <td>{{ \Carbon\Carbon::parse(@$data->expire_date)->format('d/m/Y h:i')}}</td>
                                                         <td>@if(@$data->status=='0') <span style="color:#7bd235">Active</span> @else <span style="color:#ecb403"> Expired </span> @endif</td>
                                                         <td><a href="{{url('admin/domains/view/'.$data->id)}}" class="btn btn-danger btn-pill">Manage</a></td>
                                                    </tr>
                                                    @endforeach
                                                    @endif   

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Row -->
             
        @endsection

    @section('scripts')

    <!-- Select2 js-->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>

    <!-- DATA TABLE JS-->
    <script src="{{asset('assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/dataTables.bootstrap5.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.bootstrap5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/jszip.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/responsive.bootstrap5.min.js')}}"></script>
    <script src="{{asset('assets/js/table-data.js')}}"></script>
    <!-- SWEET-ALERT JS -->
    <script src="{{asset('assets/plugins/sweet-alert/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/js/sweet-alert.js')}}"></script>

    <!-- INTERNAL Edit-Table JS -->
   <!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script>
    <script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
    <script type="text/javascript">

      $("document").ready(function () {
      $("#responsive-datatable4").dataTable({        
         language:{searchPlaceholder:"Search...",scrollX:"100%",sSearch:""},
         "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',       
      });
      var table = $('#responsive-datatable4').DataTable();
      $("#filterTable_filter.dataTables_filter").append($("#categoryFilter"));
      var categoryIndex = 0;
      $("#responsive-datatable4 th").each(function (i) {
        if ($($(this)).html() == "Status") {
          categoryIndex = i; return false;
        }
      });
      $.fn.dataTable.ext.search.push(
        function (settings, data, dataIndex) {
          var selectedItem = $('#categoryFilter').val()
          var category = data[categoryIndex];
          if (selectedItem === "" || category.includes(selectedItem)) {
            return true;
          }
          return false;
        }
      );
      $("#categoryFilter").change(function (e) {
        table.draw();
      });
      table.draw();
    });


        /*$(document).ready(function() {*/
        /*var table = $('#responsive-datatable5').DataTable({
            dom: 'Bfrtip',
            select: true,
            lengthMenu: [
                  [10, 25, 50, -1],
                  ['10 rows', '25 rows', '50 rows', 'Show all rows']
              ],
             buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
             buttons: [
                  { extend: 'pdf', text: '<i class="fas fa-file-pdf fa-1x" aria-hidden="true">PDF</i>' },
                  { extend: 'csv', text: '<i class="fas fa-file-csv fa-1x">CSV</i>' },
                  { extend: 'excel', text: '<i class="fas fa-file-excel" aria-hidden="true">EXCEL</i>' },
                  { extend: 'copy', text: '<i class="fas fa-file-copy" aria-hidden="true">COPY</i>' },
                  { extend: 'print', text: '<i class="fas fa-file-print" aria-hidden="true">PRINT</i>' },
                  'pageLength'
              ],
        });
        table.buttons().container()
              .appendTo('#datatable_wrapper .col-md-6:eq(0)');
        });*/
    </script>
    @endsection
