@extends('admin.layouts.app') @section('styles') <style>
  .list-group-item a {
    color: white;
  }
</style> @endsection @section('content')
<!-- PAGE-HEADER -->
<div class="page-header">
  <h1 class="page-title">Domain</h1>
  <div>
    <ol class="breadcrumb">
      <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li> -->
      <li class="breadcrumb-item" aria-current="page">
        <a href="javascript:void(0)">Admin</a>
      </li>
      <li class="breadcrumb-item active" aria-current="page">Domain</li>
    </ol>
  </div>
</div>
<div class="row row-sm">
  <div class="col-md-12 col-xl-10">
    <div class="card">
      <div class="card-body">
        <div class="">
          <div class="form-group">
            <label for="exampleInputEmail1" class="form-label">Manage Your Domain</label>
            <input type="domain" class="form-control" id="exampleInputEmail1" placeholder="{{@$data->domain_name}}" readonly>
          </div>
          <div class="form-group">
            <div class="col-xl-12">
              <div class="card">
                <div class="card-body">
                  <div class="panel panel-primary">
                    <div class="tab-menu-heading">
                      <div class="tabs-menu">
                        <!-- Tabs -->
                        <ul class="nav panel-tabs panel-danger">
                          <li style="margin-right: 4px;">
                            <a href="#tab13" @if(session('active_tab')=='1' ) class="active" @endif data-bs-toggle="tab">Contact Information</a>
                          </li>
                          <li style="margin-right: 4px;">
                            <a href="#tab14" data-bs-toggle="tab" @if(session('active_tab')=='2' ) class="active" @endif>
                              <span></span>Nameserver </a>
                          </li>
                          <li style="margin-right: 4px;">
                            <a href="#tab15" data-bs-toggle="tab" @if(session('active_tab')=='3' ) class="active" @endif>
                              <span></span>Invoicing Party </a>
                          </li>
                          <li style="margin-right: 4px;">
                            <a href="#tab16" data-bs-toggle="tab" @if(session('active_tab')=='4' ) class="active" @endif>
                              <span></span>Domain Transfer </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="panel-body tabs-menu-body">
                      <div class="tab-content">
                        <div class="tab-pane  @if(session('active_tab')=='1') active @endif" id="tab13">
                          <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="card">
                              <div class="card-header" style="color: white;background-color: #5a5a5a;padding: 0.2rem 0.5rem;">
                                <div class="card-title">DOMAIN</div>
                              </div>
                              <div class="card-body nameserver-hostname-border">
                                <!-- content -->
                                <div class="content">
                                  <div class="card-body">
                                    <form class="form-horizontal" action="{{url('admin/domains/update/'.Request::segment(4))}}" method="post"> @csrf <input type="hidden" name="action_page" value="contact">
                                      <div class=" row mb-4">
                                        <label for="inputName" class="col-md-3 form-label">Domain Name</label>
                                        <div class="col-md-9">
                                          <input type="text" class="form-control" id="inputName" placeholder="Domain name" name="domain_name" value="{{@$data->domain_name}}" @if(auth()->guard('admin')->user()->admin_type_id!='1') readonly @endif>
                                        </div>
                                      </div>
                                      <div class=" row mb-4">
                                        <label for="inputEmail3" class="col-md-3 form-label">Registration No</label>
                                        <div class="col-md-9">
                                          <input type="text" class="form-control" id="inputEmail3" placeholder="Registration No" name="registration_no" value="{{@$data->registration_no}}" @if(auth()->guard('admin')->user()->admin_type_id!='1') readonly @endif>
                                        </div>
                                      </div>
                                      <div class=" row mb-4">
                                        <label for="inputEmail3" class="col-md-3 form-label">Registration Date</label>
                                        <div class="col-md-9">
                                          <input type="text" class="form-control" id="registration_date" placeholder="Registration Date" name="registration_date" @if(auth()->guard('admin')->user()->admin_type_id!='1') readonly @endif>
                                        </div>
                                      </div>
                                      <div class=" row mb-4">
                                        <label for="inputEmail3" class="col-md-3 form-label">Expiry Date</label>
                                        <div class="col-md-9">
                                          <input type="text" class="form-control" id="expiry_date" placeholder="Expiry Date" name="expiry_date" @if(auth()->guard('admin')->user()->admin_type_id!='1') readonly @endif>
                                        </div>
                                      </div>
                                      <div class=" row mb-4">
                                        <label for="inputEmail3" class="col-md-3 form-label">Domain Type</label>
                                        <div class="col-md-9">
                                          <select name="domain_type" class="form-control select2-show-search form-select" data-placeholder="Choose one" @if(auth()->guard('admin')->user()->admin_type_id!='1') disabled @endif> <option label="Choose one"></option> @if(@$domain_type_list) @foreach(@$domain_type_list as $key=>$value) <option value="{{@$value->id}}" @if($value->id==$data->domain_type) selected @endif>{{@$value->domain_type}}</option> @endforeach @endif </select>
                                        </div>
                                      </div> @if(auth()->guard('admin')->user()->admin_type_id=='1') <div class="mb-0 mt-4 row justify-content-end">
                                        <div class="col-md-9">
                                          <button class="btn btn-primary">Update</button>
                                        </div>
                                      </div> @endif
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="card">
                              <div class="card-header" style="color: white;background-color: #5a5a5a;padding: 0.2rem 0.5rem;">
                                <div class="card-title">REGISTRANT</div>
                              </div>
                              <div class="card-body nameserver-hostname-border">
                                <!-- content -->
                                <div class="content">
                                  <div class="d-flex mb-2">
                                    <div>
                                      <a class="nav-link border rounded-pill chat-profile me-2" href="">
                                        <i class="fa fa-building-o"></i>
                                      </a>
                                    </div>
                                    <div class="ms-2">
                                      <p class="fs-10 fw-semibold mb-0">&nbsp;</p>
                                      <p class="fs-15 text-muted">{{@$data->registration_name}}</p>
                                    </div>
                                  </div>
                                  <div class="d-flex mb-2">
                                    <div>
                                      <a class="nav-link border rounded-pill chat-profile me-2" href="">
                                        <i class="fa fa-map-marker"></i>
                                      </a>
                                    </div>
                                    <div class="ms-2">
                                      <p class="fs-10 fw-semibold mb-0">&nbsp;</p>
                                      <p class="fs-15 text-muted">{{@$data->address}}</p>
                                    </div>
                                  </div>
                                  <div class="d-flex mb-2">
                                    <div>
                                      <a class="nav-link border rounded-pill chat-profile me-2" href="">
                                        <i class="fa fa-envelope"></i>
                                      </a>
                                    </div>
                                    <div class="ms-2">
                                      <p class="fs-10 fw-semibold mb-0">&nbsp;</p>
                                      <p class="fs-15 text-muted">{{@$data->email}}</p>
                                    </div>
                                  </div>
                                  <div class="d-flex mb-2">
                                    <div>
                                      <a class="nav-link border rounded-pill chat-profile me-2" href="">
                                        <i class="fa fa-phone"></i>
                                      </a>
                                    </div>
                                    <div class="ms-2">
                                      <p class="fs-10 fw-semibold mb-0">&nbsp;</p>
                                      <p class="fs-15 text-muted">{{@$data->phone}}</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="card">
                              <div class="card-header" style="color: white;background-color: #5a5a5a;padding: 0.2rem 0.5rem;">
                                <div class="card-title">ADMINISTRITIVE</div>
                              </div>
                              <div class="card-body nameserver-hostname-border">
                                <!-- content -->
                                <div class="content">
                                  <div class="d-flex mb-2">
                                    <div>
                                      <a class="nav-link border rounded-pill chat-profile me-2" href="">
                                        <i class="fa fa-building-o"></i>
                                      </a>
                                    </div>
                                    <div class="ms-2">
                                      <p class="fs-10 fw-semibold mb-0">&nbsp;</p>
                                      <p class="fs-15 text-muted">{{auth()->guard('admin')->user()->organization}}</p>
                                    </div>
                                  </div>
                                  <div class="d-flex mb-2">
                                    <div>
                                      <a class="nav-link border rounded-pill chat-profile me-2" href="">
                                        <i class="fa fa-map-marker"></i>
                                      </a>
                                    </div>
                                    <div class="ms-2">
                                      <p class="fs-10 fw-semibold mb-0">&nbsp;</p>
                                      <p class="fs-15 text-muted">{{auth()->guard('admin')->user()->address}}</p>
                                    </div>
                                  </div>
                                  <div class="d-flex mb-2">
                                    <div>
                                      <a class="nav-link border rounded-pill chat-profile me-2" href="">
                                        <i class="fa fa-envelope"></i>
                                      </a>
                                    </div>
                                    <div class="ms-2">
                                      <p class="fs-10 fw-semibold mb-0">&nbsp;</p>
                                      <p class="fs-15 text-muted">{{auth()->guard('admin')->user()->email}}</p>
                                    </div>
                                  </div>
                                  <div class="d-flex mb-2">
                                    <div>
                                      <a class="nav-link border rounded-pill chat-profile me-2" href="">
                                        <i class="fa fa-phone"></i>
                                      </a>
                                    </div>
                                    <div class="ms-2">
                                      <p class="fs-10 fw-semibold mb-0">&nbsp;</p>
                                      <p class="fs-15 text-muted">{{auth()->guard('admin')->user()->phone_number}}</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane @if(session('active_tab')=='2') active @endif" id="tab14">
                          <div class="row">
                            <div class="col-md-12 col-xl-6">
                              <div class="card">
                                <div class="card-header" style="color: white;background-color: #5a5a5a;padding: 0.2rem 0.5rem;">
                                  <div class="card-title">NAMESERVER 1</div>
                                </div>
                                <div class="card-body nameserver-hostname-border">
                                  <!-- content -->
                                  <div class="content">
                                    <div class="card-body">
                                      <form class="form-horizontal" action="{{url('admin/domains/update/'.Request::segment(4))}}" method="post"> @csrf <input type="hidden" name="action_page" value="nameserver1">
                                        <div class=" row mb-4">
                                          <label for="inputName" class="col-md-3 form-label">Hostname</label>
                                          <div class="col-md-9">
                                            <input type="text" class="form-control" id="inputName" placeholder="Host name" name="nameserver1" value="{{@$data->getnameserver->hostname1}}">
                                          </div>
                                        </div>
                                        <div class="mb-0 mt-4 row justify-content-end">
                                          <div class="col-md-9">
                                            <button class="btn btn-secondary">Cancel</button>
                                            <button class="btn btn-primary">Save</button>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12 col-xl-6">
                              <div class="card">
                                <div class="card-header" style="color: white;background-color: #5a5a5a;padding: 0.2rem 0.5rem;">
                                  <div class="card-title">NAMESERVER 2</div>
                                </div>
                                <div class="card-body nameserver-hostname-border">
                                  <!-- content -->
                                  <div class="content">
                                    <div class="card-body">
                                      <form class="form-horizontal" action="{{url('admin/domains/update/'.Request::segment(4))}}" method="post"> @csrf <input type="hidden" name="action_page" value="nameserver2">
                                        <div class=" row mb-4">
                                          <label for="inputName" class="col-md-3 form-label">Hostname</label>
                                          <div class="col-md-9">
                                            <input type="text" class="form-control" id="inputName" placeholder="Host name" name="nameserver1" value="{{@$data->getnameserver->hostname2}}">
                                          </div>
                                        </div>
                                        <div class="mb-0 mt-4 row justify-content-end">
                                          <div class="col-md-9">
                                            <button class="btn btn-secondary">Cancel</button>
                                            <button class="btn btn-primary">Save</button>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane @if(session('active_tab')=='3') active @endif" id="tab15">
                          <div class="row">
                            <div class="col-md-12 col-xl-6">
                              <div class="card">
                                <div class="card-header" style="color: white;background-color: #5a5a5a;padding: 0.2rem 0.5rem;">
                                  <div class="card-title">DOMAIN</div>
                                </div>
                                <div class="card-body nameserver-hostname-border">
                                  <!-- content -->
                                  <div class="content">
                                    <div class="card-body">
                                      <form class="form-horizontal">
                                        <div class=" row mb-4">
                                          <label for="inputName" class="col-md-3 form-label">Domain Name</label>
                                          <div class="col-md-9">
                                            <input type="text" class="form-control" id="inputName" placeholder="Domain name" name="domain" value="{{@$data->domain_name}}" readonly>
                                          </div>
                                        </div>
                                        <div class=" row mb-4">
                                          <label for="inputName" class="col-md-3 form-label">Registration No</label>
                                          <div class="col-md-9">
                                            <input type="text" class="form-control" id="inputName" placeholder="Registration No" name="registration_no" value="{{@$data->registration_no}}" readonly>
                                          </div>
                                        </div>
                                        <div class=" row mb-4">
                                          <label for="inputName" class="col-md-3 form-label">Registration Date</label>
                                          <div class="col-md-9">
                                            <input type="text" class="form-control" placeholder="Host name" name="registration_date" id="registration_date1" value="" readonly>
                                          </div>
                                        </div>
                                        <div class=" row mb-4">
                                          <label for="inputName" class="col-md-3 form-label">Expiry Date</label>
                                          <div class="col-md-9">
                                            <input type="text" class="form-control" placeholder="Host name" name="expiry_date" id="expiry_date1" value="" readonly>
                                          </div>
                                        </div>
                                        <div class=" row mb-4">
                                          <label for="inputName" class="col-md-3 form-label">Domain Status</label>
                                          <div class="col-md-9" style="margin-top: 10px;"> @if(@$data->status=='0') <span style="color:#7bd235"> Active</span> @else <span style="color:#ecb403"> Expired </span> @endif </div>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12 col-xl-6">
                              <div class="card">
                                <div class="card-header" style="color: white;background-color: #5a5a5a;padding: 0.2rem 0.5rem;">
                                  <div class="card-title">CHANGE INVOICING PARTY</div>
                                </div>
                                <div class="card-body nameserver-hostname-border">
                                  <!-- content -->
                                  <div class="content">
                                    <div class="card-body">
                                      <form class="form-horizontal" action="{{url('admin/domains/update/'.Request::segment(4))}}" method="post"> @csrf <input type="hidden" name="action_page" value="invoice">
                                        <div class="row mb-4">
                                          <label for="inputName" class="col-md-3 form-label">Invoicing Party</label>
                                          <div class="col-md-9">
                                            <input type="text" class="form-control" id="invoice_party" placeholder="Host name" name="invoice_party" value="{{@$data->getinvoicepartylistData(@$data->id)}}" readonly>
                                          </div>
                                        </div>
                                        <div class="form-group">
                                          <div class="row mb-4">
                                            <label for="inputName" class="col-md-3 form-label">New Invoicing Party</label>
                                            <div class="col-md-9">
                                              <select name="new_invoice" class="form-control select2-show-search form-select" required data-placeholder="Choose one">
                                                <option label="Choose one"></option> @if(@$domain_type_list) @foreach(@$newinvoice as $key=>$value) <option value="{{@$value->id}}" @if($value->id==@$data->getinvoicepartylist->invoice_party_id) selected @endif>{{@$value->name}}</option> @endforeach @endif
                                              </select>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="mb-0 mt-4 row justify-content-end">
                                          <div class="col-md-9">
                                            <button class="btn btn-primary">Change</button>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="tab-pane @if(session('active_tab')=='4') active @endif" id="tab16">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="card">
                                <div class="card-body">
                                  <form id="form" class="form-horizontal" action="{{url('admin/domains/update/'.Request::segment(4))}}" method="post"> @csrf <input type="hidden" name="action_page" value="domaintransfer">
                                    <div class="list-group">
                                      <div class="list-group-item" data-acc-step="">
                                        <h5 class="mb-0 d-flex" data-acc-title="">
                                          <span class="form-wizard-title">Registrant Info</span>
                                        </h5>
                                        <div data-acc-content="" style="display: none;">
                                          <div class="my-3">
                                            <div class=" row mb-1">
                                              <label for="inputName" class="col-md-3 form-label">Registrar</label>
                                              <div class="col-md-5">
                                                <input type="text" class="form-control" id="domain_registrar" placeholder="Registrar" name="domain_registrar" value="{{@$data->registration_name}}">
                                              </div>
                                            </div>
                                            <div class=" row mb-1">
                                              <label for="inputName" class="col-md-3 form-label">Domain Type</label>
                                              <div class="col-md-5">
                                                <select id="domain_domain_type" name="domain_domain_type" class="form-control select2-show-search form-select" data-placeholder="Choose one">
                                                  <option label="Choose one"></option> @if(@$domain_type_list) @foreach(@$domain_type_list as $key=>$value) <option value="{{@$value->id}}" @if(@$data->domain_type==$value->id) selected @endif>{{@$value->domain_type}}</option> @endforeach @endif
                                                </select>
                                              </div>
                                            </div>
                                            <div class=" row mb-1">
                                              <label for="inputName" class="col-md-3 form-label">Full Name</label>
                                              <div class="col-md-5">
                                                <input type="text" class="form-control" id="domain_full_name" placeholder="Full Name" name="domain_full_name" value="{{@$data->full_name}}">
                                              </div>
                                            </div>
                                            <div class=" row mb-1">
                                              <label for="inputName" class="col-md-3 form-label">Contact Number</label>
                                              <div class="col-md-5">
                                                <input type="text" class="form-control" id="domain_phone" placeholder="Phone" name="domain_phone" value="{{@$data->phone}}" maxlength="12" pattern="^(\+?6?01)[0-46-9]-*[0-9]{7,8}$">
                                              </div>
                                            </div>
                                            <div class=" row mb-1">
                                              <label for="inputName" class="col-md-3 form-label">Email</label>
                                              <div class="col-md-5">
                                                <input type="text" class="form-control" id="domain_email" placeholder="Email" name="domain_email" value="{{@$data->email}}" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$">
                                              </div>
                                            </div>
                                            <div class=" row mb-1">
                                              <label for="inputName" class="col-md-3 form-label">IC Number</label>
                                              <div class="col-md-5">
                                                <input type="text" class="form-control" id="domain_ic_number" placeholder="Ic number" name="domain_ic_number" value="{{@$data->ic_number}}">
                                              </div>
                                            </div>
                                            <div class=" row mb-1">
                                              <label for="inputName" class="col-md-3 form-label">DOB</label>
                                              <div class="col-md-5">
                                                <input type="text" class="form-control" id="domain_dob" placeholder="DOB" name="domain_dob" value="{{@$data->dob}}">
                                              </div>
                                            </div>
                                            <div class=" row mb-1">
                                              <label for="inputName" class="col-md-3 form-label">Gender</label>
                                              <div class="col-md-6">
                                                <label class="custom-control custom-radio">
                                                  <input type="radio" class="custom-control-input" name="domain_gender" value="male" @if(@$data->gender=='male') checked="" @endif> <span class="custom-control-label">Male</span>
                                                </label>
                                                <label class="custom-control custom-radio">
                                                  <input type="radio" class="custom-control-input" name="domain_gender" value="female" @if(@$data->gender=='female') checked="" @endif> <span class="custom-control-label">Female</span>
                                                </label>
                                              </div>
                                            </div>
                                            <div class=" row mb-1">
                                              <label for="inputName" class="col-md-3 form-label">Address</label>
                                              <div class="col-md-5">
                                                <textarea class="form-control mb-4" id="domain_address" name="domain_address" placeholder="Address" rows="4">{{@$data->address}}</textarea>
                                              </div>
                                            </div>
                                            <div class=" row mb-1">
                                              <label for="inputName" class="col-md-3 form-label">Postal code</label>
                                              <div class="col-md-5">
                                                <input type="text" class="form-control" id="domain_postal_code" placeholder="Post code" name="domain_postal_code" value="{{@$data->postal_code}}">
                                              </div>
                                            </div>
                                            <div class=" row mb-1">
                                              <label for="inputName" class="col-md-3 form-label">City</label>
                                              <div class="col-md-5">
                                                <input type="text" class="form-control" id="domain_city" placeholder="City" name="domain_city" value="{{@$data->city}}">
                                              </div>
                                            </div>
                                            <div class=" row mb-1">
                                              <label for="inputName" class="col-md-3 form-label">State</label>
                                              <div class="col-md-5">
                                                <input type="text" class="form-control" id="domain_state" placeholder="State" name="domain_state" value="{{@$data->state}}">
                                              </div>
                                            </div>
                                            <div class=" row mb-1">
                                              <label for="inputName" class="col-md-3 form-label">Country</label>
                                              <div class="col-md-5">
                                                <select id="domain_country" name="domain_country" class="form-control select2-show-search form-select" data-placeholder="Choose one">
                                                  <option label="Choose one"></option> @if(@$country) @foreach(@$country as $key=>$val) <option value="{{@$val->id}}" @if($val->id==$data->country) selected @endif>{{@$val->nicename}}</option> @endforeach @endif
                                                </select>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="list-group-item" data-acc-step="">
                                        <h5 class="mb-0 d-flex" data-acc-title="">
                                          <span class="form-wizard-title">Nameserver</span>
                                        </h5>
                                        <div data-acc-content="" style="display: none;">
                                          <div class="my-3">
                                            <div class="row">
                                              <div class="col-md-12 col-xl-6">
                                                <div class="card">
                                                  <div class="card-header" style="color: white;background-color: #5a5a5a;padding: 0.2rem 0.5rem;">
                                                    <div class="card-title">NAMESERVER 1</div>
                                                  </div>
                                                  <div class="card-body nameserver-hostname-border">
                                                    <!-- content -->
                                                    <div class="content">
                                                      <div class="card-body">
                                                        <form class="form-horizontal">
                                                          <div class=" row mb-4">
                                                            <label for="inputName" class="col-md-3 form-label">Hostname</label>
                                                            <div class="col-md-9">
                                                              <input type="text" class="form-control" id="domain_hostname1" placeholder="Host name" name="domain_hostname1" value="{{@$data->getnameserver->hostname1}}">
                                                            </div>
                                                          </div>
                                                        </form>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                              <div class="col-md-12 col-xl-6">
                                                <div class="card">
                                                  <div class="card-header" style="color: white;background-color: #5a5a5a;padding: 0.2rem 0.5rem;">
                                                    <div class="card-title">NAMESERVER 2</div>
                                                  </div>
                                                  <div class="card-body nameserver-hostname-border">
                                                    <!-- content -->
                                                    <div class="content">
                                                      <div class="card-body">
                                                        <div class=" row mb-4">
                                                          <label for="inputName" class="col-md-3 form-label">Hostname</label>
                                                          <div class="col-md-9">
                                                            <input type="text" class="form-control" id="domain_hostname2" placeholder="Host name" name="domain_hostname2" value="{{@$data->getnameserver->hostname2}}">
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="list-group-item open" data-acc-step="">
                                        <h5 class="mb-0 d-flex" data-acc-title="">
                                          <span class="form-wizard-title">Confirm</span>
                                        </h5>
                                        <div data-acc-content="" style="">
                                          <div class="my-3">
                                            <div class="card">
                                              <div class="card-header" style="background-color:#5A5A5A;padding: 0.2rem 0.5rem;">
                                                <h4 class="card-title" style="color:white;">Registrant Info</h4>
                                              </div>
                                              <div class="card-body nameserver-hostname-border">
                                                <div class="row">
                                                  <div class="col-xl-6">
                                                    <div class="card">
                                                      <div class="card-body">
                                                        <ul class="list-group">
                                                          <li class="list-group-item">
                                                            <b>Registrar :</b>
                                                            <div class="material-switch pull-right">
                                                              <span id="confirm_registrar"></span>
                                                            </div>
                                                          </li>
                                                          <li class="list-group-item">
                                                            <b>Domain Type :</b>
                                                            <div class="material-switch pull-right">
                                                              <span id="confirm_domain_type"></span>
                                                            </div>
                                                          </li>
                                                          <li class="list-group-item">
                                                            <b>Full Name :</b>
                                                            <div class="material-switch pull-right">
                                                              <span id="confirm_fullname"></span>
                                                            </div>
                                                          </li>
                                                          <li class="list-group-item">
                                                            <b>Contact Number :</b>
                                                            <div class="material-switch pull-right">
                                                              <span id="confirm_contact_number"></span>
                                                            </div>
                                                          </li>
                                                          <li class="list-group-item">
                                                            <b>Email :</b>
                                                            <div class="material-switch pull-right">
                                                              <span id="confirm_email"></span>
                                                            </div>
                                                          </li>
                                                          <li class="list-group-item">
                                                            <b>IC Number :</b>
                                                            <div class="material-switch pull-right">
                                                              <span id="confirm_ic_number"></span>
                                                            </div>
                                                          </li>
                                                          <li class="list-group-item">
                                                            <b>DOB :</b>
                                                            <div class="material-switch pull-right">
                                                              <span id="confirm_dob"></span>
                                                            </div>
                                                          </li>
                                                          <li class="list-group-item">
                                                            <b>Gender :</b>
                                                            <div class="material-switch pull-right">
                                                              <span id="confirm_gender"></span>
                                                            </div>
                                                          </li>
                                                        </ul>
                                                      </div>
                                                    </div>
                                                  </div>
                                                  <div class="col-xl-6">
                                                    <div class="card">
                                                      <div class="card-body">
                                                        <ul class="list-group">
                                                          <li class="list-group-item">
                                                            <b>Address :</b>
                                                            <div class="material-switch pull-right">
                                                              <span id="confirm_address"></span>
                                                            </div>
                                                          </li>
                                                          <li class="list-group-item">
                                                            <b>Post Code :</b>
                                                            <div class="material-switch pull-right">
                                                              <span id="confirm_postcode"></span>
                                                            </div>
                                                          </li>
                                                          <li class="list-group-item">
                                                            <b>City :</b>
                                                            <div class="material-switch pull-right">
                                                              <span id="confirm_city"></span>
                                                            </div>
                                                          </li>
                                                          <li class="list-group-item">
                                                            <b>State :</b>
                                                            <div class="material-switch pull-right">
                                                              <span id="confirm_state"></span>
                                                            </div>
                                                          </li>
                                                          <li class="list-group-item">
                                                            <b>Country :</b>
                                                            <div class="material-switch pull-right">
                                                              <span id="confirm_country"></span>
                                                            </div>
                                                          </li>
                                                        </ul>
                                                      </div>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="card">
                                              <div class="card-header" style="background-color:#5A5A5A;padding: 0.2rem 0.5rem;">
                                                <h4 class="card-title" style="color:white;">Nameserver</h4>
                                              </div>
                                              <div class="card-body nameserver-hostname-border">
                                                <form class="form-horizontal">
                                                  <div class="form-row">
                                                    <div class="col-md-5 mb-5">
                                                      <label class="form-label" for="example-email">Nameserver 1</label> Hostname : <span id="confirm_hostname1"></span>
                                                    </div>
                                                    <div class="col-md-5 mb-5">
                                                      <label class="form-label">Nameserver 2</label> Hostname : <span id="confirm_hostname2"></span>
                                                    </div>
                                                  </div>
                                                </form>
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label class="custom-control custom-checkbox-md mb-0">
                                                <input type="checkbox" class="custom-control-input" name="domain_agreement" value="1" @if(@$data->domain_agreement=='1') checked @endif>
                                                <span class="custom-control-label">I have read and agreed to the Domain Name Agreement</span>
                                                <span id="agreement_error"></span>
                                              </label>

                                            </div>
                                           
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- End Row --> @endsection @section('scripts')
<!-- Select2 js-->
<script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
<!-- DATA TABLE JS-->
<script src="{{asset('assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/js/dataTables.bootstrap5.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/js/buttons.bootstrap5.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/js/jszip.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/pdfmake/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/pdfmake/vfs_fonts.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('assets/plugins/datatable/responsive.bootstrap5.min.js')}}"></script>
<script src="{{asset('assets/js/table-data.js')}}"></script>
<!-- SWEET-ALERT JS -->
<script src="{{asset('assets/plugins/sweet-alert/sweetalert.min.js')}}"></script>
<script src="{{asset('assets/js/sweet-alert.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-steps/jquery.steps.min.js')}}"></script>
<script src="{{asset('assets/plugins/accordion-Wizard-Form/jquery.accordion-wizard.min.js')}}"></script>
<script src="{{asset('assets/js/form-wizard.js?v='.time())}}"></script>
<!-- SELECT2 JS -->
<script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
<script src="{{asset('assets/js/select2.js')}}"></script>
<!-- INTERNAL Edit-Table JS -->
<!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script><script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
<script type="text/javascript">
  $("document").ready(function() {
    //acc-step-number badge rounded-pill bg-primary me-1
    //$('.acc-step-number').addClass('bg-danger').removeClass('bg-primary');
    //$('a.float-end2').addClass('btn-danger').removeClass('btn-primary');
    //$('a.float-end').addClass('btn-danger').removeClass('btn-primary');
    //$('a.btn-light').addClass('btn-secondary');
    //append selected values
    $(".float-end").on('click', function(event) {
      event.stopPropagation();
      event.stopImmediatePropagation();
      $('#confirm_registrar').text($('#domain_registrar').val());
      $('#confirm_domain_type').text($('#domain_domain_type option:selected').text());
      $('#confirm_fullname').text($('#domain_full_name').val());
      $('#confirm_contact_number').text($('#domain_phone').val());
      $('#confirm_email').text($('#domain_email').val());
      $('#confirm_ic_number').text($('#domain_ic_number').val());
      $('#confirm_dob').text($('#domain_dob').val());
      $('#confirm_gender').text($('input[name="domain_gender"]:checked').val());
      $('#confirm_address').text($('#domain_address').val());
      $('#confirm_postcode').text($('#domain_postal_code').val());
      $('#confirm_city').text($('#domain_city').val());
      $('#confirm_state').text($('#domain_state').val());
      $('#confirm_country').text($('#domain_country :selected').html());
      $('#confirm_hostname1').text($('#domain_hostname1').val());
      $('#confirm_hostname2').text($('#domain_hostname2').val());
    });
    //append selected values
    //datetimepicker
    var registration_date = "{{@$data->getregistrationdates()}}";
    var expiry_date = "{{@$data->getexpirydates()}}";
    var domain_dob = "{{@$data->getdob()}}";
    $(function() {
      $('#registration_date').datetimepicker({
        //defaultDate: defaulsst,  
        pickTime: false,
        minView: 2,
        format: 'dd M yyyy',
        autoclose: true,
      });
      $('#expiry_date').datetimepicker({
        defaultDate: new Date(),
        pickTime: false,
        minView: 2,
        format: 'dd M yyyy',
        autoclose: true,
      });
      $('#domain_dob').datetimepicker({
        pickTime: false,
        minView: 2,
        format: 'dd-mm-yyyy',
        autoclose: true,
      });
      /*$('#expiry_date1').datetimepicker({
          defaultDate: new Date(),       
          pickTime: false,
          minView: 2,
          format: 'dd M yyyy',
          autoclose: true,
       });*/
    });
    $('#registration_date').val(registration_date);
    $('#expiry_date').val(expiry_date);
    $('#registration_date1').val(registration_date);
    $('#expiry_date1').val(expiry_date);
    $('#domain_dob').val(domain_dob);
    //datetimepicker
    $("#responsive-datatable4").dataTable({
      language: {
        searchPlaceholder: "Search...",
        scrollX: "100%",
        sSearch: ""
      },
      "sDom": '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
    });
    var table = $('#responsive-datatable4').DataTable();
    $("#filterTable_filter.dataTables_filter").append($("#categoryFilter"));
    var categoryIndex = 0;
    $("#responsive-datatable4 th").each(function(i) {
      if ($($(this)).html() == "Status") {
        categoryIndex = i;
        return false;
      }
    });
    $.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
      var selectedItem = $('#categoryFilter').val()
      var category = data[categoryIndex];
      if (selectedItem === "" || category.includes(selectedItem)) {
        return true;
      }
      return false;
    });
    $("#categoryFilter").change(function(e) {
      table.draw();
    });
    table.draw();
  });

  $( "#form" ).validate({
            submitHandler : function(form) {
            form.submit();
        },
          rules: {
            domain_agreement: {
              required: true,
            },
          },
            messages: {        
                domain_agreement: {
                    required: "This field is required",
                },
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "domain_agreement") {
                    error.appendTo("#agreement_error").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                }/*else if (element.attr("name") == "password_confirmation") {
                    error.appendTo("#errorpassword1").css('color','#FF0000').css("fontSize", "14px").css('float','center');
                }*/else {
                    error.insertAfter(element);
                }                
               },
        });
  /*$(document).ready(function() {*/
  /*var table = $('#responsive-datatable5').DataTable({
            dom: 'Bfrtip',
            select: true,
            lengthMenu: [
                  [10, 25, 50, -1],
                  ['10 rows', '25 rows', '50 rows', 'Show all rows']
              ],
             buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
             buttons: [
                  { extend: 'pdf', text: '
                                                                                                                                <i class="fas fa-file-pdf fa-1x" aria-hidden="true">PDF</i>' },
                  { extend: 'csv', text: '
                                                                                                                                <i class="fas fa-file-csv fa-1x">CSV</i>' },
                  { extend: 'excel', text: '
                                                                                                                                <i class="fas fa-file-excel" aria-hidden="true">EXCEL</i>' },
                  { extend: 'copy', text: '
                                                                                                                                <i class="fas fa-file-copy" aria-hidden="true">COPY</i>' },
                  { extend: 'print', text: '
                                                                                                                                <i class="fas fa-file-print" aria-hidden="true">PRINT</i>' },
                  'pageLength'
              ],
        });
        table.buttons().container()
              .appendTo('#datatable_wrapper .col-md-6:eq(0)');
        });*/
</script> @endsection