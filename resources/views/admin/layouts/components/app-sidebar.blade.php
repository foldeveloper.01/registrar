            <!--app-sidebar-->
            <div class="sticky">
                <div class="app-sidebar__overlay" data-bs-toggle="sidebar"></div>
                <div class="app-sidebar">
                    <div class="side-header">
                        <a class="header-brand1" href="{{url('admin')}}">
                            <img  class="header-brand-img light-logo1" alt="logo"  src="{{asset('storage/images/settings/'.@$setting->logo)}}"/>
                        </a>
                        <!-- LOGO -->
                    </div>
                    <div class="main-sidemenu">
                        <div class="slide-left disabled" id="slide-left"><svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191" width="24" height="24" viewBox="0 0 24 24"><path d="M13.293 6.293 7.586 12l5.707 5.707 1.414-1.414L10.414 12l4.293-4.293z"/></svg></div>
                        <ul class="side-menu">
                            <li class="sub-category">
                                <h3>Dashboard</h3>
                            </li>

                             @if(Auth()->guard('admin')->user()->admin_type_id=='1')
                            <!-- <li class="slide">
                                <a class="side-menu__item @if(Request::segment(1)=='admin' && Request::segment(2)=='') active @endif" data-bs-toggle="slide" href="{{url('admin')}}"><i class="side-menu__icon fe fe-home"></i><span class="side-menu__label">Dashboard</span></a>
                            </li>    -->                        
                          
                             <li class="slide @if(Request::segment(2)=='settings') is-expanded @endif">
                                <a class="side-menu__item @if(Request::segment(2)=='settings') active @endif header_tit" data-bs-toggle="slide" href="javascript:void(0)"><i class="side-menu__icon fe fe-settings fa-spin"></i><span class="side-menu__label">Settings</span><i class="angle fe fe-chevron-right"></i></a>
                                <ul class="slide-menu @if(Request::segment(2)=='admins') open @endif">
                                    <li class="side-menu-label1"><a href="javascript:void(0)">Settings</a></li>
                                    <li><a href="{{url('admin/settings')}}" class="slide-item @if(Request::segment(2)=='settings') active @endif">Site Settings</a></li>
                                    <li><a href="{{url('admin/smtpsettings')}}" class="slide-item @if(Request::segment(2)=='settingss') active @endif">SMTP Setting</a></li>
                                </ul>
                            </li>                       

                            <li class="slide @if(Request::segment(2)=='admins') is-expanded @endif">
                                <a class="side-menu__item @if(Request::segment(2)=='admins') active @endif " data-bs-toggle="slide" href="javascript:void(0)"><i class="side-menu__icon fe fe-users"></i><span class="side-menu__label">Admin</span><i class="angle fe fe-chevron-right"></i></a>
                                <ul class="slide-menu @if(Request::segment(2)=='admins') open @endif">
                                    <li class="side-menu-label1"><a href="javascript:void(0)">Admin</a></li>
                                    <li><a href="{{url('admin/admins')}}" class="slide-item @if(Request::segment(2)=='admins') active @endif"> Admin Listing</a></li>                                   
                                    <li><a href="{{url('admin/useractivity')}}" class="slide-item @if(Request::segment(2)=='useractivity') active @endif">Audit Trail</a></li>
                                    <li><a href="{{url('admin/loginhistroy')}}" class="slide-item @if(Request::segment(2)=='loginhistroy') active @endif">Login History</a></li>
                                    <li><a href="{{url('admin/accesslevel')}}" class="slide-item @if(Request::segment(2)=='accesslevel') active @endif">Admin Access Level</a></li>
                                </ul>
                            </li>
                            <li class="slide @if(Request::segment(2)=='domains' || Request::segment(2)=='dns') is-expanded @endif">
                                <a class="side-menu__item @if(Request::segment(2)=='domains' || Request::segment(2)=='dns') active @endif " data-bs-toggle="slide" href="javascript:void(0)"><i class="side-menu__icon fe fe-aperture"></i><span class="side-menu__label">Domain</span><i class="angle fe fe-chevron-right"></i></a>
                                <ul class="slide-menu @if(Request::segment(2)=='domains' || Request::segment(2)=='dns') open @endif">
                                    <li class="side-menu-label1"><a href="javascript:void(0)">Domain</a></li>
                                    <li><a href="{{url('admin/domains')}}" class="slide-item @if(Request::segment(2)=='domains') active @endif"> Domain Listing</a></li> 
                                    <li><a href="{{url('admin/dns')}}" class="slide-item @if(Request::segment(2)=='dns') active @endif">DNS</a></li>
                                </ul>
                            </li>

                            <li class="slide @if(Request::segment(2)=='paymenthistroy' || Request::segment(2)=='renewal') is-expanded @endif">
                                <a class="side-menu__item @if(Request::segment(2)=='paymenthistroy' || Request::segment(2)=='renewal') active @endif " data-bs-toggle="slide" href="javascript:void(0)"><i class="side-menu__icon fe fe-bold"></i><span class="side-menu__label">Billing</span><i class="angle fe fe-chevron-right"></i></a>
                                <ul class="slide-menu @if(Request::segment(2)=='paymenthistroy' || Request::segment(2)=='renewal') open @endif">
                                    <li class="side-menu-label1"><a href="javascript:void(0)">Billing</a></li>
                                    <li><a href="{{url('admin/paymenthistroy')}}" class="slide-item @if(Request::segment(2)=='paymenthistroy') active @endif"> Payment History</a></li>                                   
                                    <li><a href="{{url('admin/renewal')}}" class="slide-item @if(Request::segment(2)=='renewal') active @endif">Renewal</a></li>
                                </ul>
                            </li>
                            @else
                            <!-- customer dealers reseller -->
                            <li class="slide @if(Request::segment(2)=='domains' || Request::segment(2)=='dns') is-expanded @endif">
                                <a class="side-menu__item @if(Request::segment(2)=='domains' || Request::segment(2)=='dns') active @endif " data-bs-toggle="slide" href="javascript:void(0)"><i class="side-menu__icon fe"></i><span class="side-menu__label">Domain</span><i class="angle fe fe-chevron-right"></i></a>
                                <ul class="slide-menu @if(Request::segment(2)=='domains' || Request::segment(2)=='dns') open @endif">
                                    <li class="side-menu-label1"><a href="javascript:void(0)">Domain</a></li>
                                    <li><a href="{{url('admin/domains')}}" class="slide-item @if(Request::segment(2)=='domains') active @endif"> Domain Listing</a></li> 
                                    <li><a href="{{url('admin/dns')}}" class="slide-item @if(Request::segment(2)=='dns') active @endif">Dns</a></li>
                                </ul>
                            </li>

                            <li class="slide @if(Request::segment(2)=='paymenthistroy' || Request::segment(2)=='renewal') is-expanded @endif">
                                <a class="side-menu__item @if(Request::segment(2)=='paymenthistroy' || Request::segment(2)=='renewal') active @endif " data-bs-toggle="slide" href="javascript:void(0)"><i class="side-menu__icon fe"></i><span class="side-menu__label">Billing</span><i class="angle fe fe-chevron-right"></i></a>
                                <ul class="slide-menu @if(Request::segment(2)=='paymenthistroy' || Request::segment(2)=='renewal') open @endif">
                                    <li class="side-menu-label1"><a href="javascript:void(0)">Billing</a></li>
                                    <li><a href="{{url('admin/paymenthistroy')}}" class="slide-item @if(Request::segment(2)=='paymenthistroy') active @endif"> Payment History</a></li>                                   
                                    <li><a href="{{url('admin/renewal')}}" class="slide-item @if(Request::segment(2)=='renewal') active @endif">Renewal</a></li>
                                </ul>
                            </li>
                            @endif

                         
                        </ul>
                        <div class="slide-right" id="slide-right"><svg xmlns="http://www.w3.org/2000/svg" fill="#7b8191" width="24" height="24" viewBox="0 0 24 24"><path d="M10.707 17.707 16.414 12l-5.707-5.707-1.414 1.414L13.586 12l-4.293 4.293z"/></svg></div>
                    </div>
                </div>
                <!--/APP-SIDEBAR-->
            </div>
            <!--app-sidebar-->
