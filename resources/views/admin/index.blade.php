@extends('admin.layouts.app')

    @section('styles')

    @endsection

        @section('content')

                        <!-- PAGE-HEADER -->
                        <div class="page-header">
                            <h1 class="page-title">Dashboard</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin')}}">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 -->

                      


                    <div class="row">
                        <form id="search_domain_form" class="" action="{{url('admin')}}" method="post">
                            @csrf
                            <div class="col-sm-12 col-md-12 col-lg-6 col-xl-6 ">                          
                                <div class="card-body">
                                    <div class="input-group">
                                     <input type="text" id="domain_search" name="domain_search" class="form-control border-end-1" placeholder="Search ..." style="margin-left: -37px" required>
                                            <button class="btn btn-default py-3" style="background-color: #ed1f26;color: white;">Search Domain</button>
                                    </div>
                                </div>                            
                            </div>
                        </form>

                    </div>

                        <div class="row dashboard-rows">                            
                        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-3 ">
                            <a href="{{url('admin/domains')}}">
                            <div class="card service">
                                <div class="card-body">
                                    <div class="item-box text-center">
                                        <div class=" text-center text-danger-gradient mb-4"><i class="fa fa-dedent"></i></div>
                                        <div class="item-box-wrap">
                                            <h5 class="mb-2 dashboard-span-color">Manage Domain</h5>
                                            <p class="text-muted mb-0">Add, Edit, Transfer and Delete Domain of your preference</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                    
                        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-3">
                            <a href="{{url('admin/dns')}}">
                            <div class="card service">
                                <div class="card-body">
                                    <div class="item-box text-center">
                                        <div class=" text-center text-danger-gradient mb-4"><i class="fa fa-dribbble"></i></div>
                                        <div class="item-box-wrap">
                                            <h5 class="mb-2 dashboard-span-color">DNS</h5>
                                            <p class="text-muted mb-0">Add, Edit, Transfer and Delete Domain of your preference</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-3">
                            <a href="{{url('admin/paymenthistroy')}}">
                            <div class="card service">
                                <div class="card-body">
                                    <div class="item-box text-center">
                                        <div class=" text-center text-danger-gradient mb-4"><i class="fa fa-history"></i></div>
                                        <div class="item-box-wrap">
                                            <h5 class="mb-2 dashboard-span-color">Order History</h5>
                                            <p class="text-muted mb-0">Past and pending orders, receipts and refunds</p>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </a>
                        </div>                        
                    </div>



                    <div class="row dashboard-rows">
                        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-3 ">
                            <div class="card service">
                                <div class="card-body">
                                    <div class="item-box text-center">
                                        <div class=" text-center  text-danger-gradient mb-4"><i class="fa fa-ils"></i></div>
                                        <div class="item-box-wrap">
                                            <h5 class="mb-2 dashboard-span-color">Renewal & Billing</h5>
                                            <p class="text-muted mb-0">Expiring products, billing dates, and recurring payments</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6 col-xl-3">
                            @if(Auth()->guard('admin')->user()->admin_type_id=='1')
                            <a href="{{url('admin/admins')}}">
                            @else
                            <a href="{{url('admin/admins/view/'.Auth()->guard('admin')->user()->id)}}">
                            @endif
                            <div class="card service">
                                <div class="card-body">
                                    <div class="item-box text-center">
                                        <div class=" text-center text-danger-gradient mb-4"><i class="icon icon-people"></i></div>
                                        <div class="item-box-wrap">
                                            <h5 class="mb-2 dashboard-span-color">Profile</h5>
                                            <p class="text-muted mb-0">Username, password, support PIN and 2-step verification</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           </a>
                        </div>
                        <!-- <div class="col-sm-12 col-md-12 col-lg-6 col-xl-3">
                            <div class="card service">
                                <div class="card-body">
                                    <div class="item-box text-center">
                                        <div class=" text-center text-success mb-4"><i class="icon icon-people"></i></div>
                                        <div class="item-box-wrap">
                                            <h5 class="mb-2">Profile</h5>
                                            <p class="text-muted mb-0">Username, password, support PIN and 2-step verification</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  -->                       
                    </div>
                        <!-- ROW-1 END -->

                        <!-- ROW-2 -->
                        <div class="row">




                          
                            <!-- COL END -->





                         



                            <!-- COL END -->
                        </div>
                        <!-- ROW-2 END -->                      
                        </div>
                        <!-- ROW-3 END -->
                        <!-- ROW-4 -->                        
                        <!-- ROW-4 END -->

        @endsection

    @section('scripts')

    <!-- SPARKLINE JS-->
    <script src="{{asset('assets/plugins/jquery-sparkline/jquery.sparkline.min.js')}}"></script>

    <!-- CHART-CIRCLE JS-->
    <script src="{{asset('assets/plugins/circle-progress/circle-progress.min.js')}}"></script>

    <!-- INTERNAL SELECT2 JS -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>

    <!-- PIETY CHART JS-->
    <script src="{{asset('assets/plugins/peitychart/jquery.peity.min.js')}}"></script>
    <script src="{{asset('assets/plugins/peitychart/peitychart.init.js')}}"></script>

    <!-- INTERNAL CHARTJS CHART JS-->
    <script src="{{asset('assets/plugins/chart/Chart.bundle.js')}}"></script>
    <script src="{{asset('assets/plugins/chart/rounded-barchart.js')}}"></script>
    <script src="{{asset('assets/plugins/chart/utils.js')}}"></script>

    <!-- INTERNAL SELECT2 JS -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>

    <!-- INTERNAL Data tables js-->
    <script src="{{asset('assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/dataTables.bootstrap5.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/js/table-data.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/responsive.bootstrap5.min.js')}}"></script>
        <script src="{{asset('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.bootstrap5.min.js')}}"></script>
       <script src="{{asset('assets/plugins/datatable/js/jszip.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>

    <!-- INTERNAL APEXCHART JS -->
    <script src="{{asset('assets/js/apexcharts.js')}}"></script>
    <script src="{{asset('assets/plugins/apexchart/irregular-data-series.js')}}"></script>

    <!-- C3 CHART JS -->
    <script src="{{asset('assets/plugins/charts-c3/d3.v5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/charts-c3/c3-chart.js')}}"></script>

    <!-- CHART-DONUT JS -->
    <script src="{{asset('assets/js/charts.js')}}"></script>

    <!-- INTERNAL Flot JS -->
    <script src="{{asset('assets/plugins/flot/jquery.flot.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/jquery.flot.fillbetween.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/chart.flot.sampledata.js')}}"></script>
    <script src="{{asset('assets/plugins/flot/dashboard.sampledata.js')}}"></script>

    <!-- INTERNAL Vector js -->
    <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
    <script src="{{asset('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

    <!-- INTERNAL INDEX JS -->
    <script src="{{asset('assets/js/index.js')}}"></script>
    <script src="{{asset('assets/js/index1.js')}}"></script>
    <script type="text/javascript">
        $("#responsive-datatable1").DataTable({language:{searchPlaceholder:"Search...",scrollX:"100%",sSearch:""}});
    </script>

    @endsection
