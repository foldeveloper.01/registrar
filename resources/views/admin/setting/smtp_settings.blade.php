@extends('admin.layouts.app')

    @section('styles')

    @endsection

        @section('content')

                           <!-- PAGE-HEADER -->
                           <div class="page-header">
                            <h1 class="page-title">SMTP Setting</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{url('admin')}}">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Smtp Setting</li>
                                </ol>
                            </div>
                        </div>
                        <!-- PAGE-HEADER END -->

                        <!-- ROW-1 OPEN -->
                        <div class="row">
                          
                            <form id="create_user" action="{{url('admin/smtpsettings')}}" method="post" enctype="multipart/form-data">
                                 @csrf
                            <div class="col-xl-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">SMTP Setting</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group">
                                                    <label for="exampleInputname">Server Name</label>
                                                    <input type="text" value="@if(@$settingData->server_name!==''){{@$settingData->server_name}}@endif"  name="server_name" class="form-control" id="exampleInputname" placeholder="Server Name">
                                                </div>
                                                @if($errors->has('server_name'))
                                                        <div class="error">{{ $errors->first('server_name') }}</div>
                                                   @endif
                                            </div>
                                            <div class="col-lg-6 col-md-12">
                                                <div class="form-group"> 
                                                    <label for="exampleInputname1">Port</label>
                                                    <input type="text" value="@if(@$settingData->port!==''){{@$settingData->port}}@endif" name="port" class="form-control" id="exampleInputname1" placeholder="Port">
                                                </div>
                                                @if($errors->has('port'))
                                                        <div class="error">{{ $errors->first('port') }}</div>
                                                   @endif
                                            </div>
                                        </div>
                                         <div class="row">
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group"> 
                                            <label for="exampleInputEmail1">User Name</label>
                                            <input type="user_name" name="user_name" value="@if(@$settingData->user_name!==''){{@$settingData->user_name}}@endif" class="form-control" id="exampleInputEmail1" placeholder="User Name">
                                            @if($errors->has('user_name'))
                                                        <div class="error">{{ $errors->first('user_name') }}</div>
                                            @endif
                                        </div>
                                        </div>
                                        <div class="col-lg-6 col-md-12">
                                            <div class="form-group"> 
                                            <label for="exampleInputnumber">Password</label>
                                            <input type="text" name="password" value="@if(@$settingData->password!==''){{@$settingData->password}}@endif" class="form-control" id="exampleInputnumber" placeholder="Password">
                                             @if($errors->has('password'))
                                                        <div class="error">{{ $errors->first('password') }}</div>
                                            @endif
                                        </div>
                                        </div>
                                    </div>
                                        <div class="row">
                                        <div class="col-lg-6 col-md-12">
                                            <label for="exampleInputnumber">From Name</label>
                                            <input type="text" name="from_name" value="@if(@$settingData->from_name!==''){{@$settingData->from_name}}@endif" class="form-control" id="exampleInputnumber" placeholder="From Name">
                                            @if($errors->has('from_name'))
                                                        <div class="error">{{ $errors->first('from_name') }}</div>
                                            @endif
                                        </div>

                                        <div class="col-lg-6 col-md-12">
                                            <label for="exampleInputnumber">Reply Email</label>
                                            <input type="text" name="reply_email" value="@if(@$settingData->reply_email!==''){{@$settingData->reply_email}}@endif" class="form-control" id="exampleInputnumber" placeholder="Reply Email">
                                            @if($errors->has('reply_email'))
                                                        <div class="error">{{ $errors->first('reply_email') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                        
                                    </div>
                                    <div class="card-footer text-end">
                                        <a href="{{url('admin')}}" class="btn btn-danger my-1">Back</a>
                                        <button class="btn btn-success my-1" value="submit">Save</button>                                        
                                    </div>
                                </div>                              
                            </div>
                           </form>
                        </div>
                        <!-- ROW-1 CLOSED -->

        @endsection

    @section('scripts')

    <!-- INTERNAL SELECT2 JS -->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>

    @endsection
