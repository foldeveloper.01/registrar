@extends('admin.layouts.custom-app')
    @section('styles')
    @endsection
    @section('class')
    <!-- BACKGROUND-IMAGE -->
    <div class="login-img">        
    @endsection
        @section('content')
                <!-- CONTAINER OPEN -->
                <div class="col-login mx-auto">
                    <div class="text-center">
                         <img src="{{asset('storage/images/settings/'.@$setting->logo)}}" class="header-brand-img m-0" alt="">
                    </div>
                </div>

                <!-- CONTAINER OPEN -->
                <div class="container-login100">
                    <div class="wrap-login100 p-6">
                        <form class="login100-form validate-form" action="{{route('forgotpassword')}}" method="post" id="forgot_password_form">
                             @csrf
                             @if(Session::has('success'))
                               <div class="alert alert-error text-center">
                                 {{Session::get('success')}}
                               </div>
                             @endif
                            <span class="login100-form-title pb-5">
                                Reset Password
                            </span>
                            <p class="text-muted">Enter your email address linked with your  account and <br> we will send an email to reset your password.</p>
                            <div class="wrap-input100 validate-input input-group" data-bs-validate="Valid email is required: ex@abc.xyz">
                                <a href="javascript:void(0)" class="input-group-text bg-white text-muted">
                                    <i class="zmdi zmdi-email" aria-hidden="true"></i>
                                </a>
                                <input id="email" name="email" class="input100 form-control @error('email') is-invalid @enderror" value="{{old('email')}}" type="email" placeholder="Email"><br>                                
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror                               
                            </div>
                            @if(@$messages)
                                <span style="color:red;">{{ @$messages }}</span>
                                @endif
                            <span id="email-error"></span>
                            <div class="submit">
                                <button type="submit" class="btn btn-primary w-100" tabindex="4">Submit</button>
                            </div>
                            <div class="text-center mt-4">
                                <p class="text-dark mb-0">Already have an account?<a class="text-primary ms-1" href="{{url('admin/login')}}">login</a></p>
                            </div>
                           <!--  <label class="login-social-icon"><span>OR</span></label> -->
                           <!--  <div class="d-flex justify-content-center">
                                <a href="javascript:void(0)">
                                    <div class="social-login me-4 text-center">
                                        <i class="fa fa-google"></i>
                                    </div>
                                </a>
                                <a href="javascript:void(0)">
                                    <div class="social-login me-4 text-center">
                                        <i class="fa fa-facebook"></i>
                                    </div>
                                </a>
                                <a href="javascript:void(0)">
                                    <div class="social-login text-center">
                                        <i class="fa fa-twitter"></i>
                                    </div>
                                </a>
                            </div> -->
                        </form>
                    </div>
                </div>
                <!-- CONTAINER CLOSED -->

        @endsection

    @section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
        // just for the demos, avoids form submit
        jQuery.validator.setDefaults({
          debug: true,
          success: "valid"
        });
        $( "#forgot_password_form" ).validate({
            submitHandler : function(form) {
            form.submit();
        },
          rules: {
            email: {
              required: true,
              email: true,
            }
          },
            messages: {        
                email: {
                    required: "Please enter valid email"
                },
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "email") {
                    error.appendTo("#email-error").css('color','#dc3545').css("fontSize", "14px").css('float','center');
                }else {
                    error.insertAfter(element);
                }                
              }
           });
        });
    </script>

    @endsection
