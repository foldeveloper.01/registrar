@extends('admin.layouts.app')
    @section('styles')
    @endsection
        @section('content')

                           <!-- PAGE-HEADER -->
                           <div class="page-header">
                            <h1 class="page-title">Admin Access Level</h1>
                            <div>
                                <ol class="breadcrumb">
                                    <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">Tables</a></li> -->
                                    <li class="breadcrumb-item" aria-current="page"><a href="javascript:void(0)">Admin</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Admin Access Level</li>
                                </ol>
                            </div>
                        </div>

                        <div class="row row-sm">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Admin Access Level List</h3>
                                    </div>
                                    <div class="card-body">                                        
               
               <div class="row">
                  <div class="col-md-12">
                     <div class="table-responsive">
                        <table class="table table-role">
                           <thead class="table-primary">
                              <tr>
                                 <th>Access Category</th>
                                 @foreach(@$designation_list as $key=>$value)
                                 <th>{{@$value->type}}</th>
                                 @endforeach
                                 
                              </tr>
                           </thead>
                           <tbody>
                              @foreach($output_data as $key=>$menu)
                              <tr>
                            <?php $option= array('Create', 'Update', 'Delete', 'View'); ?>
                            <td><h4>{{$key}}</h4>
                                @foreach($menu as $keys=>$value)
                                 <span><h6>{{$value['label_name']}}</h6></span>
                                  @foreach($option as $key=>$opt)
                                  <span>&nbsp;&nbsp;&nbsp;&nbsp;<h9>{{$opt}}</h9></span><br>
                                  @endforeach<br>
                                @endforeach
                            </td>




                            <td> <br>
                                 @foreach($menu as $keys=>$value)
                                 <?php
                                         $admin_create = '0';
                                         $admin_update = '0';
                                         $admin_delete = '0';
                                         $admin_view = '0';                            
                                  if(isset($menu[$keys]['checked_value'][0])){
                                  if(isset($menu[$keys]['checked_value'][0]['admin'])){
                                          $admin_create = $menu[$keys]['checked_value'][0]['admin']['create'];
                                          $admin_update = $menu[$keys]['checked_value'][0]['admin']['update'];
                                          $admin_delete = $menu[$keys]['checked_value'][0]['admin']['delete'];
                                          $admin_view = $menu[$keys]['checked_value'][0]['admin']['view'];
                                        }                                  
                                        }                                  
                                 ?>
                                    
                                    <label class="custom-control custom-checkbox">
                                       <input class="custom-control-input check-front" type="checkbox" name="admin_{{$value['id']}}" id="admin_{{$value['id']}}" value="admin_{{$value['id']}}">
                                    </label>

                                    <label class="custom-control custom-checkbox">
                                       <input @if($admin_create=='1') checked @endif class="custom-control-input check-front" type="checkbox" name="admin_create_{{$value['id']}}" id="admin_create_{{$value['id']}}" value="admin_create_{{$value['id']}}">
                                       <span class="custom-control-label"></span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                       <input @if($admin_update=='1') checked @endif class="custom-control-input check-front" type="checkbox" name="admin_update_{{$value['id']}}" id="admin_update_{{$value['id']}}" value="admin_update_{{$value['id']}}">
                                       <span class="custom-control-label"></span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                       <input @if($admin_delete=='1') checked @endif class="custom-control-input check-front" type="checkbox" name="admin_delete_{{$value['id']}}" id="admin_delete_{{$value['id']}}" value="admin_delete_{{$value['id']}}">
                                       <span class="custom-control-label"></span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                       <input @if($admin_view=='1') checked @endif class="custom-control-input check-front" type="checkbox" name="admin_view_{{$value['id']}}" id="admin_view_{{$value['id']}}" value="admin_view_{{$value['id']}}">
                                       <span class="custom-control-label"></span>
                                    </label>                                                            
                                 @endforeach
                             </td>





                            <td> <br>
                                 @foreach($menu as $keys=>$value)
                                 <?php
                                         $manager_create = '0';
                                         $manager_update = '0';
                                         $manager_delete = '0';
                                         $manager_view = '0';                            
                                  if(isset($menu[$keys]['checked_value'][0])){
                                  if(isset($menu[$keys]['checked_value'][0]['manager'])){
                                          $manager_create = $menu[$keys]['checked_value'][0]['manager']['create'];
                                          $manager_update = $menu[$keys]['checked_value'][0]['manager']['update'];
                                          $manager_delete = $menu[$keys]['checked_value'][0]['manager']['delete'];
                                          $manager_view = $menu[$keys]['checked_value'][0]['manager']['view'];
                                        }                                  
                                        }
                                 ?>
                                    
                                <label class="custom-control custom-checkbox">
                                       <input class="custom-control-input check-front" type="checkbox" name="manager_{{$value['id']}}" id="manager_{{$value['id']}}" value="manager_{{$value['id']}}">
                                    </label>
                                <label class="custom-control custom-checkbox">
                                       <input @if($manager_create=='1') checked @endif class="custom-control-input check-front" type="checkbox" name="manager_create_{{$value['id']}}" id="manager_create_{{$value['id']}}" value="manager_create_{{$value['id']}}">
                                       <span class="custom-control-label"></span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                       <input @if($manager_update=='1') checked @endif  class="custom-control-input check-front" type="checkbox" name="manager_update_{{$value['id']}}" id="manager_update_{{$value['id']}}" value="manager_update_{{$value['id']}}">
                                       <span class="custom-control-label"></span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                       <input @if($manager_delete=='1') checked @endif class="custom-control-input check-front" type="checkbox" name="manager_delete_{{$value['id']}}" id="manager_delete_{{$value['id']}}" value="manager_delete_{{$value['id']}}">
                                       <span class="custom-control-label"></span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                       <input @if($manager_view=='1') checked @endif class="custom-control-input check-front" type="checkbox" name="manager_view_{{$value['id']}}" id="manager_view_{{$value['id']}}" value="manager_view_{{$value['id']}}">
                                       <span class="custom-control-label"></span>
                                    </label>
                                 @endforeach
                             </td>







                             <td> <br>
                                 @foreach($menu as $keys=>$value)
                                 <?php
                                         $clerk_create = '0';
                                         $clerk_update = '0';
                                         $clerk_delete = '0';
                                         $clerk_view = '0';
                                  if(isset($menu[$keys]['checked_value'][0])){
                                  if(isset($menu[$keys]['checked_value'][0]['clerk'])){
                                          $clerk_create = $menu[$keys]['checked_value'][0]['clerk']['create'];
                                          $clerk_update = $menu[$keys]['checked_value'][0]['clerk']['update'];
                                          $clerk_delete = $menu[$keys]['checked_value'][0]['clerk']['delete'];
                                          $clerk_view = $menu[$keys]['checked_value'][0]['clerk']['view'];
                                        }
                                    }
                                 ?>
                                 <label class="custom-control custom-checkbox">
                                       <input class="custom-control-input check-front" type="checkbox" name="clerk_{{$value['id']}}" id="clerk_{{$value['id']}}" value="clerk_{{$value['id']}}">
                                    </label>

                                 <label class="custom-control custom-checkbox">
                                       <input @if($clerk_create=='1') checked @endif class="custom-control-input check-front" type="checkbox" name="clerk_create_{{$value['id']}}" id="clerk_create_{{$value['id']}}" value="clerk_create_{{$value['id']}}">
                                       <span class="custom-control-label"></span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                       <input @if($clerk_update=='1') checked @endif class="custom-control-input check-front" type="checkbox" name="clerk_update_{{$value['id']}}" id="clerk_update_{{$value['id']}}" value="clerk_update_{{$value['id']}}">
                                       <span class="custom-control-label"></span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                       <input @if($clerk_delete=='1') checked @endif class="custom-control-input check-front" type="checkbox" name="clerk_delete_{{$value['id']}}" id="clerk_delete_{{$value['id']}}" value="clerk_delete_{{$value['id']}}">
                                       <span class="custom-control-label"></span>
                                    </label>
                                    <label class="custom-control custom-checkbox">
                                       <input @if($clerk_view=='1') checked @endif class="custom-control-input check-front" type="checkbox" name="clerk_view_{{$value['id']}}" id="clerk_view_{{$value['id']}}" value="clerk_view_{{$value['id']}}">
                                       <span class="custom-control-label"></span>
                                    </label>
                                 @endforeach
                             </td>



                                 
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div><!-- Table Responsive -->
                  </div>
               </div>

               
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Row -->
             
        @endsection

    @section('scripts')

    <!-- Select2 js-->
    <script src="{{asset('assets/plugins/select2/select2.full.min.js')}}"></script>
    <script src="{{asset('assets/js/select2.js')}}"></script>

    <!-- DATA TABLE JS-->
    <script src="{{asset('assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/dataTables.bootstrap5.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.bootstrap5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/jszip.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/js/buttons.colVis.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/plugins/datatable/responsive.bootstrap5.min.js')}}"></script>
    <script src="{{asset('assets/js/table-data.js')}}"></script>
    <!-- SWEET-ALERT JS -->
    <script src="{{asset('assets/plugins/sweet-alert/sweetalert.min.js')}}"></script>
    <script src="{{asset('assets/js/sweet-alert.js')}}"></script>

    <!-- INTERNAL Edit-Table JS -->
   <!--  <script src="{{asset('assets/plugins/edit-table/bst-edittable.js')}}"></script>
    <script src="{{asset('assets/plugins/edit-table/edit-table.js')}}"></script> -->
    <script type="text/javascript">
        $(document).ready(function() {
            $(".check-front").click(function() {
                var checked = $(this).prop("checked");
                var value = $(this).val();
                $.ajax({
                 url:"{{ url('admin/accesslevel/useraccesslevel') }}",
                 method: 'post',
                 data: {value:value, checked:checked, _token: '{{csrf_token()}}'},
                 dataType: 'json',
                 success: function(response){
                    console.log(response.success);
                 }
             });
            });
        });
    </script>
    @endsection
