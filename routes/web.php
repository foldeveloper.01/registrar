<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminAuthController;
use App\Http\Controllers\Admin\SettingsController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\UserLoginHistroyController;
use App\Http\Controllers\Admin\UserActivityController;
use App\Http\Controllers\Admin\AccessLevelController;
use App\Http\Controllers\Admin\DomainController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\PaymentHistroyController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

       
    //Clear Cache facade value:
    Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
    });

    //Reoptimized class loader:
    Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
    });

    //Route cache:
    Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
    });

    //Clear Route cache:
    Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
    });

    //Clear View cache:
    Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
    });

    //Clear Config cache:
    Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
    });

    Route::get('generate', function (){
    \Illuminate\Support\Facades\Artisan::call('storage:link');
    echo 'ok';
    });
   
    
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/login', [AdminAuthController::class, 'getLogin'])->name('adminLogin');
    Route::post('/login', [AdminAuthController::class, 'postLogin'])->name('adminLoginPost');
    Route::get('/signup', [AdminAuthController::class, 'signup'])->name('signup');
    Route::get('/registerr', [AdminAuthController::class, 'register'])->name('registerr');
    Route::get('/logout', [AdminAuthController::class, 'getLogout']);
    Route::get('/forgotpassword', [AdminAuthController::class, 'forgotPassword'])->name('forgotpassword');
    Route::post('/forgotpassword', [AdminAuthController::class, 'gentrateUserPassword'])->name('forgotpassword');

    Route::group(['middleware' => 'adminauth'], function () {
        //dashboard
        Route::get('/', [DashboardController::class, 'dashboard'])->name('adminDashboard');
        Route::post('/', [DashboardController::class, 'searchDomain'])->name('adminDashboard');
        //user list
        Route::get('/admins', [UserController::class, 'userslist']);
        Route::get('/admins/create', [UserController::class, 'createuser']);
        Route::post('/admins/save', [UserController::class, 'saveuser']);
        Route::get('/admins/update/{id}', [UserController::class, 'updateuser']);
        Route::post('/admins/update/{id}', [UserController::class, 'edituser']);
        Route::any('/admins/delete/{id}', [UserController::class, 'deleteuser']);
        Route::get('/admins/view/{id}', [UserController::class, 'viewuser']);
        Route::any('/admins/emailSend/{id}', [UserController::class, 'ResetUserPasswordToMail']);
        Route::post('/admins/emailSend1/{id}', [UserController::class, 'ResetUserPasswordToMail1']);
        Route::post('/admins/myaccountupdate/{id}', [UserController::class, 'myaccountupdate']);
        Route::get('/admins/checkCurrentPassword/{id}/{password}', [UserController::class, 'checkCurrentPassword']);
        //settings
        Route::get('/settings', [SettingsController::class, 'settings']);
        Route::post('/settings', [SettingsController::class, 'savesettings']);
        Route::get('/smtpsettings', [SettingsController::class, 'smtpsettings']);
        Route::post('/smtpsettings', [SettingsController::class, 'savesmtpsettings']);
       
        //user menu access leval
        Route::get('/accesslevel', [AccessLevelController::class, 'listdata']);
        Route::post('/accesslevel/useraccesslevel', [AccessLevelController::class, 'savedata']);
        //end       
        //user login histroy
        Route::get('/loginhistroy', [UserLoginHistroyController::class, 'listdata']);
        //user login histroy
        Route::get('/useractivity', [UserActivityController::class, 'listdata']);
        
        //domain listing
        Route::get('/domains', [DomainController::class, 'listdata']);
        Route::get('/domains/view/{id}', [DomainController::class, 'getdata']);
        Route::post('/domains/update/{id}', [DomainController::class, 'updateData']);
        Route::any('/domains/checkout', [DomainController::class, 'checkout']);
        //dns 
        Route::get('/dns', [DomainController::class, 'getdns']);
        Route::post('/dns', [DomainController::class, 'savedns']);
        Route::get('/dns/getdomainservername/{id}', [DomainController::class, 'getdomainservername']);
        //payment controller
        Route::get('/paymenthistroy', [PaymentHistroyController::class, 'listdata']);
        Route::get('/paymenthistroy/view/{id}', [PaymentHistroyController::class, 'viewdata']);

    });
});
Auth::routes();
//front end
Route::get('/', [FrontController::class, 'index']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');