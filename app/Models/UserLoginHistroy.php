<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Admin;
use App\Models\Admintypes;

class UserLoginHistroy extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'user_login_history';
    protected $fillable = [
        'user_id', 'last_login_at', 'created_at', 'updated_at', 'last_login_ip',
    ];

    public function userDetail()
    {
        return $this->hasOne(Admin::class,'id','user_id');
    }

    public function GetAdminType($value)
    {
        $data ='';
        if(Admintypes::where('id',$value)->count()!=0){
            return $data= Admintypes::findOrFail($value)->type;
        }
       return $data;
    }
    
   
}
