<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Admintypes;
use App\Traits\ModelEventLogger;

class Admin extends Authenticatable
{
    use HasFactory, Notifiable, ModelEventLogger;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $table = 'admin';
    protected $guard = 'admin';
    protected $fillable = [
        'firstname', 'address', 'additional_details', 'organization', 'organization_type','lastname', 'image', 'email', 'phone_number', 'password', 'admin_type_id', 'status', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function setStatusAttribute($value)
    {
       $this->attributes['status'] = 0;
    }
    public function GetAdminType($value)
    {
       return Admintypes::findOrFail($value)->type;
    }
    public function AdminTyps()
    {
        return $this->hasOne(Admintypes::class, 'id', 'admin_type_id');
    }
}
