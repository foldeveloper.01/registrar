<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Traits\Multitenantable;
use App\Models\DomainNameserver;
use App\Models\DomainInVoicePartyList;
use App\Models\InVoicePartyList;

class Domain extends Authenticatable
{
    use HasFactory, Notifiable, Multitenantable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'domains';
    protected $fillable = [
        'domain_name', 'registration_name', 'registration_no', 'registration_date', 'expire_date', 'domain_type',  'address', 'email', 'phone', 'ic_number', 'dob', 'full_name', 'postal_code', 'city', 'state', 'country', 'status', 'user_id', 'gender', 'created_at', 'updated_at', 'domain_agreement'
    ];    
    
    public function getregistrationdates()
    {
        return date('d M Y', strtotime(explode(' ',$this->registration_date)[0]));
    }
    public function getexpirydates()
    {
        return date('d M Y', strtotime(explode(' ',$this->expire_date)[0]));
    }
    public function getdob()
    {  
        if($this->dob!=''){
          return date('d-m-Y', strtotime(explode(' ',$this->dob)[0]));
        }
        return '';
        
    }
    public function getnameserver()
    {
       return $this->hasOne(DomainNameserver::class, 'domain_id', 'id');
    }
    public function getinvoicepartylist()
    {
       return $this->hasOne(DomainInVoicePartyList::class, 'domain_id', 'id');
    }
    public function getinvoicepartylistData($domain_id)
    {
                $check_data = DomainInVoicePartyList::where('domain_id',$domain_id)->first();
                if(is_object($check_data)){
                    return InVoicePartyList::findOrFail($check_data->invoice_party_id)->name;
                }else{
                    return $this->domain_name;
                }
    }
   
}
