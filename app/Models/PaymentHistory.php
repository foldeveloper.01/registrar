<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\PaymentHistory;
use App\Models\PaymentTypes;
use Carbon\Carbon;
use App\Traits\Multitenantable;

class PaymentHistory extends Authenticatable
{
    use HasFactory, Notifiable, Multitenantable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'domains_payment_histroy';
    protected $fillable = [
        'domain_id', 'user_id', 'order_id', 'receipt_number', 'payment_mode', 'payment_status', 'amount', 'expiry_at', 'created_at', 'updated_at'
    ];    
    
    public function getPayinvoiceData()
    {
        return $this->hasOne(Domain::class, 'id', 'domain_id');
    }

     public function getExpiryAtAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $value)
                                    ->format('d-m-Y');
    }
    public function getCreatedAtAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $value)
                                    ->format('d-m-Y');
    }

    public function getPaymentMode()
    {
        return $this->hasOne(PaymentTypes::class, 'id', 'payment_mode');
    }
   
}
