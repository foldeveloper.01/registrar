<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\MenuLists;
use App\Models\UserAccessLevel;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;


class AccessLevelController extends Controller
{
    //index
    public function listdata(){        
        $designation_list = Admintypes::orderBy('id','asc')->where('id','!=', '1')->where('status', '0')->get();
        $menu_list = MenuLists::orderBy('id','asc')->get();
        $output_data=[];
        foreach($menu_list as $key=>$data){
            $checked_value = [];
            $updated_data = UserAccessLevel::where('menu_id',$data->id)->first();
            if(is_object($updated_data)){
                    $loop= UserAccessLevel::where('menu_id', $data->id)->get();
                    $get_ids=[];
                    foreach($loop as $keys=>$val){
                        //admin
                        if($val->admin_type_id==2){
                            $get_ids['admin'] = ['create'=>0,'update'=>0,'delete'=>0,'view'=>0];
                        if($val->permission_create==1){
                            $get_ids['admin']['create']=1;
                        }if($val->permission_update==1){
                            $get_ids['admin']['update']=1;
                        }if($val->permission_delete==1){
                            $get_ids['admin']['delete']=1;
                        }if($val->permission_view==1){
                            $get_ids['admin']['view']=1;
                        }    
                      }
                      //admin 
                      //manager
                        if($val->admin_type_id==3){
                            $get_ids['manager'] = ['create'=>0,'update'=>0,'delete'=>0,'view'=>0];
                        if($val->permission_create==1){
                            $get_ids['manager']['create']=1;
                        }if($val->permission_update==1){
                            $get_ids['manager']['update']=1;
                        }if($val->permission_delete==1){
                            $get_ids['manager']['delete']=1;
                        }if($val->permission_view==1){
                            $get_ids['manager']['view']=1;
                        }    
                      }
                      //manager
                      //clerk
                        if($val->admin_type_id==4){
                            $get_ids['clerk'] = ['create'=>0,'update'=>0,'delete'=>0,'view'=>0];
                        if($val->permission_create==1){
                            $get_ids['clerk']['create']=1;
                        }if($val->permission_update==1){
                            $get_ids['clerk']['update']=1;
                        }if($val->permission_delete==1){
                            $get_ids['clerk']['delete']=1;
                        }if($val->permission_view==1){
                            $get_ids['clerk']['view']=1;
                        }    
                      }
                      //clerk                   
                    }                    
                    $checked_value[] = $get_ids;
            }
            $output_data[$data->menu_name][]= ['id'=>$data->id, 'menu_url'=>$data->menu_url, 'label_name'=>$data->label_name, 'checked_value'=>$checked_value];
        }
        //dd($output_data);
        return view('admin.user_access_level.user_access_level_list',compact('designation_list','output_data'));
    }
    public function savedata(Request $request){
        $data = explode('_',$request->value);        
        if($data[0]=='admin'){
           $admin_type_id = Admintypes::where('type','Administrator')->first()->id;
           $menu_id = $data[2];
        }else if($data[0]=='manager'){
           $admin_type_id = Admintypes::where('type','Manager')->first()->id;
           $menu_id = $data[2];
        }else if ($data[0]=='clerk') {
           $admin_type_id = Admintypes::where('type','Clerk')->first()->id;
           $menu_id = $data[2];
        }  
        $check_old_data = UserAccessLevel::where('admin_type_id', $admin_type_id)->where('menu_id', $menu_id)->count();
        if($check_old_data==0){
            $create = new UserAccessLevel();
            $create->admin_type_id = $admin_type_id;
            $create->menu_id = $menu_id;
            if($data[1]=='create'){
              $create->permission_create = ($request->checked=='true') ? 1 : 0 ;
            }else if($data[1]=='update'){
              $create->permission_update = ($request->checked=='true') ? 1 : 0 ;
            }else if ($data[1]=='delete') {
              $create->permission_delete = ($request->checked=='true') ? 1 : 0 ;
            }else {
              $create->permission_view = ($request->checked=='true') ? 1 : 0 ;
            }
              $create->save();
        }else{
            $create = UserAccessLevel::where('admin_type_id',$admin_type_id)->where('menu_id',$menu_id)->first();
            if($data[1]=='create'){
              $create->permission_create = ($request->checked=='true') ? 1 : 0 ;
            }else if($data[1]=='update'){
              $create->permission_update = ($request->checked=='true') ? 1 : 0 ;
            }else if ($data[1]=='delete') {
              $create->permission_delete = ($request->checked=='true') ? 1 : 0 ;
            }else {
              $create->permission_view = ($request->checked=='true') ? 1 : 0 ;
            }
            $create->save();
        }
        return response()->json(['success'=>'Updated']);
    }

}