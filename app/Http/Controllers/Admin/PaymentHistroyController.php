<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Quote;
use App\Models\UserFeedback;
use App\Models\PaymentHistory;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class PaymentHistroyController extends Controller
{
    //index
    public function listdata(Request $request){
       /* $data = new PaymentHistory();
        $data->domain_id='1';
        $data->payment_mode='1';
        $data->order_id=$this->getOrderNumber();
        $data->receipt_number=$this->RandomReceptNumber();
        $data->payment_status='success';
        $data->amount='100.00';
        $data->save();*/
        $lists = PaymentHistory::orderBy('id', 'DESC')->get();
        return view('admin.paymenthistroy.list',compact('lists'));
    }
    public function viewdata($id){
        $data = PaymentHistory::findOrFail($id);
        //dd($data->getPayinvoiceData->domain_name);
        return view('admin.paymenthistroy.invoice',compact('data'));
    }
    
    public function getOrderNumber() {
        return 'ORD#'.PaymentHistory::count()+1;
    }
    public function RandomReceptNumber() {
    $alphabet = '1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return 'REC'.implode($pass); //turn the array into a string
    }

}