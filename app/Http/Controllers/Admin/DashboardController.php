<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Quote;
use App\Models\UserFeedback;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class DashboardController extends Controller
{
    //index
    public function dashboard(Request $request){
        //https://api.ote-godaddy.com/v1/domains/available?domain=poooooo&checkType=FAST&forTransfer=false
       /* $result = $this->godaddyRequest('test',"GET","/v1/domains/available?domain=heeeellllo.co&checkType=FULL&forTransfer=false");
            echo '<pre>';
            print_r($result);
            echo '</pre>';
            exit;*/
        return view('admin.index');
    }
    public function searchDomain(Request $request){
        $search_key = $request->domain_search;
        $request->session()->put('domain_search', $search_key);
        return view('admin.domain.searchindex',compact('search_key'));
    }    

    public function godaddyRequest($mode,$request,$command,$postdata=[]){

        if($mode=='test'){
        $access_key = '3mM44UcgtKBfPE_4PtcPCgLF5HrL6piavs98b';
        $secret_key = 'GKj2kFqFYFvgDdzTRGu3gB';
        $api_domain = 'https://api.ote-godaddy.com';
        }else{
        $access_key = 'your-live-key';
        $secret_key = 'your-live-secret';
        $api_domain = 'https://api.godaddy.com';
        }
        $postdatajson = '{}';
        if(count($postdata)>0)
        $postdatajson = json_encode($postdata);        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $api_domain.$command);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,false);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdatajson);
        curl_setopt($ch, CURLOPT_HTTPHEADER,
        ['Authorization: sso-key '.$access_key.':'.$secret_key]);
        $result = curl_exec($ch);
        curl_close($ch);
        return json_decode($result, true);
        }
}