<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\UserLoginHistroy;
use App\Models\Domain;
use App\Models\DomainTypes;
use App\Models\DomainNameserver;
use App\Models\DomainInVoicePartyList;
use App\Models\InVoicePartyList;
use App\Models\Country;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;

class DomainController extends Controller
{
    //index
    public function listdata(Request $request){        
        //policy
        /* $user = auth()->guard('admin')->user();
         $user->can('view', UserLoginHistroy::class);*/
        //policy
        $request->session()->forget('active_tab');
        $list = Domain::orderBy('id', 'DESC')->get();
        return view('admin.domain.list',compact('list'));
    }
    public function getdata(Request $request,$id){
        //policy
        /* $user = auth()->guard('admin')->user();
         $user->can('view', UserLoginHistroy::class);*/
        //policy
        if(!$request->session()->has('active_tab')){
          $request->session()->put('active_tab',1);
        }
        $active_tab = '1';
        $country = Country::all();
        $domain_type_list = DomainTypes::where('status','0')->get();
        $data = Domain::findOrFail($id);
        $newinvoice = InVoicePartyList::where('status','0')->get();
        return view('admin.domain.view',compact('data','active_tab','domain_type_list','newinvoice','country'));
    }
    public function checkout(Request $request){
        //dd(session('domain_search'));
        return view('admin.domain.checkout');
    }

    public function updateData(Request $request, $id)
    {
        //dd($request->action_page);
        //contact tab        
        if($request->action_page =='contact'){
            $validated = Validator::make($request->all(),[
                'domain_name' => 'required|max:200',
                'registration_date' => 'required',
                'registration_no' => 'required',
                'expiry_date' => 'required',
                'domain_type' => 'required|exists:domains_type,id',
                 ]);
                if ($validated->fails()) {
                    return redirect()
                                ->back()
                                ->withErrors($validated)
                                ->withInput()
                                ->with('error','Updated failed');
                }
            $data = Domain::findOrFail($id);
            $data->domain_name = $request->domain_name;
            $data->domain_type = $request->domain_type; 
            $data->registration_no = $request->registration_no;
            $data->registration_date = date('Y-m-d',strtotime(implode('-',explode(' ',$request->registration_date))));
            $data->expire_date = date('Y-m-d',strtotime(implode('-',explode(' ',$request->expiry_date))));
            $data->save();
            $request->session()->put('active_tab',1);
            //return redirect()->back()->with('success','Updated sucessfully.');
            /*$active_tab = '1';
            $domain_type_list = DomainTypes::where('status','0')->get();
            $data = Domain::findOrFail($id);
            $newinvoice = InVoicePartyList::where('status','0')->get();
            $country = Country::all();
            return view('admin.domain.view',compact('data','active_tab','domain_type_list','newinvoice','country'))->with('success','Updated sucessfully.');*/
        }elseif($request->action_page=='nameserver1' || $request->action_page=='nameserver2'){
             //nameserver
            //dd($request->action_page);
             $validated = Validator::make($request->all(),[
                'nameserver1' => 'required|max:200',
                 ]);
                if ($validated->fails()) {
                    return redirect()
                                ->back()
                                ->withErrors($validated)
                                ->withInput()
                                ->with('error','Updated failed');
                }
           //nameserver
            $check_data = DomainNameserver::where('domain_id',$id)->first();
            if(is_object($check_data)){
                if($request->action_page=='nameserver1')
                   $check_data->hostname1 = $request->nameserver1;         
                if($request->action_page=='nameserver2')
                   $check_data->hostname2 = $request->nameserver1;                
                $check_data->save();
            }else{
                $data = new DomainNameserver();
                if($request->action_page=='nameserver1')
                   $data->hostname1 = $request->nameserver1;         
                if($request->action_page=='nameserver2')
                   $data->hostname2 = $request->nameserver1;
                $data->domain_id=$id;
                $data->save();
            }
            $request->session()->put('active_tab',2);
            /*$active_tab = '2';
            $domain_type_list = DomainTypes::where('status','0')->get();
            $data = Domain::findOrFail($id);
            $newinvoice = InVoicePartyList::where('status','0')->get();
            $country = Country::all();
            return view('admin.domain.view',compact('data','active_tab','domain_type_list','newinvoice','country'));*/
            //return redirect()->back()->with('success','Updated sucessfully.');
        }elseif($request->action_page=='invoice'){
            if($request->new_invoice!=null){
                 $check_data = DomainInVoicePartyList::where('domain_id',$id)->first();
                if(is_object($check_data)){
                    $check_data->invoice_party_id = $request->new_invoice;                
                    $check_data->save();
                }else{
                    $data = new DomainInVoicePartyList();
                    $data->invoice_party_id = $request->new_invoice;                   
                    $data->domain_id=$id;
                    $data->save();
                }
            }
            $request->session()->put('active_tab',3);
           /* $active_tab = '3';
            $domain_type_list = DomainTypes::where('status','0')->get();
            $data = Domain::findOrFail($id);
            $newinvoice = InVoicePartyList::where('status','0')->get();
            $country = Country::all();
            return view('admin.domain.view',compact('data','active_tab','domain_type_list','newinvoice','country'));*/
            //return redirect()->back()->with('success','Updated sucessfully.');
        }elseif($request->action_page=='domaintransfer'){
            //dd(date('Y-m-d',strtotime($request->domain_dob)));
            $data = Domain::findOrFail($id);
            $data->registration_name = $request->domain_registrar;
            $data->domain_type = $request->domain_domain_type; 
            $data->full_name = $request->domain_full_name;
            $data->phone = $request->domain_phone;
            $data->email = $request->domain_email;
            $data->ic_number = $request->domain_ic_number;
            if($request->domain_dob!=null || $request->domain_dob!='' ){
                $data->dob = date('Y-m-d',strtotime($request->domain_dob)); 
            }
            $data->gender = $request->domain_gender;
            $data->address = $request->domain_address;
            $data->postal_code = $request->domain_postal_code;
            $data->city = $request->domain_city;
            $data->state = $request->domain_state;
            $data->country = $request->domain_country;
            $data->domain_agreement = $request->domain_agreement;
            $data->save();
            $check_data = DomainNameserver::where('domain_id',$id)->first();
            if(is_object($check_data)){
                if($request->domain_hostname1!='')
                   $check_data->hostname1 = $request->domain_hostname1;         
                if($request->domain_hostname2!='')
                   $check_data->hostname2 = $request->domain_hostname2;                
                $check_data->save();
            }else{
                $data = new DomainNameserver();
                if($request->domain_hostname1!='')
                   $data->hostname1 = $request->domain_hostname1;         
                if($request->domain_hostname2!='')
                   $data->hostname2 = $request->domain_hostname2;
                $data->domain_id=$id;
                $data->save();
            }
            $request->session()->put('active_tab',4);
            /*$active_tab = '4';
            $domain_type_list = DomainTypes::where('status','0')->get();
            $data = Domain::findOrFail($id);
            $newinvoice = InVoicePartyList::where('status','0')->get();
            $country = Country::all();
            return view('admin.domain.view',compact('data','active_tab','domain_type_list','newinvoice','country'));*/

        }        
        return redirect()->back()->with('success','Updated sucessfully.');
    }

    public function getdns(Request $request){
        $list = Domain::orderBy('domain_name', 'DESC')->get();
        return view('admin.domain.dnslist',compact('list'));
    }
    public function savedns(Request $request){
        if(isset($request->action_id)){
            $data=DomainNameserver::findOrFail($request->action_id);
            $data->hostname1=$request->nameserver1;
            $data->save();
        }
        if(isset($request->action_id1)){
            $data=DomainNameserver::findOrFail($request->action_id1);
            $data->hostname2=$request->nameserver2;
            $data->save();
        }
        return redirect()->back()->with(['success'=>'Updated sucessfully.','selected_id'=>$data->domain_id],'Updated sucessfully.');        
    }
    public function getdomainservername($id){
        if($id==0){
         return response()->json($data=''); exit;
        }
        $data['data'] = DomainNameserver::where('domain_id',$id)->first();
        return response()->json($data); exit;
    }

}