<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\Admin;
use App\Models\Admintypes;
use App\Models\Settings;
use App\Models\SmtpSettings;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class SettingsController extends Controller
{
    public function settings(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', Settings::class);
        //policy
        $settingData = Settings::first();
        return view('admin.setting.settings',compact('settingData'));
    }
    
    //create settings
    public function savesettings(Request $request)
    {    //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', Settings::class);
        //policy
        $validated = Validator::make($request->all(),[
        'sitename' => 'required|max:200',
        'email' => 'required|max:100',
        'number' => 'required',
        'address' => 'required',
        'logo' => 'nullable|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'favicon' => 'nullable|mimes:jpeg,png,jpg,gif,svg,x-icon,icon,ico|max:2048'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()
                        ->with('error','updated failed');
        }
        //create data
        $data = Settings::count();
        if($data==0){
           $user = new Settings();            
         if($request->hasFile('logo'))
         {
            $path = $request->file('logo')->store('public/images/settings');
            $user->logo  = explode ("/", $path)[3];
         }
         if($request->hasFile('favicon'))
         {
            $path = $request->file('favicon')->store('public/images/settings');
            $user->favicon  = explode ("/", $path)[3];
         }        
        $user->site_name = $request->sitename;
        $user->site_link  = ($request->sitelink) ? $request->sitelink : '';
        $user->email    = $request->email;
        $user->phone     = $request->number;
        $user->fax  = ($request->fax) ? $request->fax :'';
        $user->address  = $request->address;
        $user->meta_descriptions  = ($request->meta_descriptions) ? $request->meta_descriptions : '';
        $user->meta_keywords  = ($request->meta_keywords) ? $request->meta_keywords :'';
        $user->meta_author  = ($request->meta_author) ? $request->meta_author :'';
        $user->save();
        }else{
        $user = Settings::first();
         if($request->hasFile('logo'))
         {
            $path = $request->file('logo')->store('public/images/settings');
            $user->logo  = explode ("/", $path)[3];
         }
         if($request->hasFile('favicon'))
         {
            $path = $request->file('favicon')->store('public/images/settings');
            $user->favicon  = explode ("/", $path)[3];
         }
        $user->site_name = $request->sitename;
        $user->site_link  = $request->sitelink;
        $user->email    =  ($request->email) ? $request->email:'';
        $user->phone     = ($request->number) ? $request->number:'';
        $user->fax  =  ($request->fax) ? $request->fax:'';
        $user->address  = ($request->address) ? $request->address:'';
        $user->meta_descriptions  = ($request->meta_descriptions)?:'';
        $user->meta_keywords  =  ($request->meta_keywords)?:'';
        $user->meta_author  =  ($request->meta_author)?:'';
        $user->save();
        }
        return redirect('admin/settings')->with('success','updated sucessfully.');
    }
    

     public function smtpsettings(){
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('view', SmtpSettings::class);
        //policy
        $settingData = SmtpSettings::first();
        return view('admin.setting.smtp_settings',compact('settingData'));
    }

     //create settings
    public function savesmtpsettings(Request $request)
    {
        //policy
         $user = auth()->guard('admin')->user();
         $user->can('update', SmtpSettings::class);
        //policy
        $validated = Validator::make($request->all(),[
        'server_name' => 'required|max:200',
        'port' => 'required|max:200',
        'user_name' => 'required|max:200',
        'password' => 'required|max:200',
        'from_name' => 'required|max:200',
        'reply_email' => 'required|max:200'
         ]);
        if ($validated->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validated)
                        ->withInput()
                        ->with('error','updated failed');
        }
        //create data
        $data = SmtpSettings::count();
        if($data==0){
        $user = new SmtpSettings();
        $user->server_name = $request->server_name;
        $user->port  = ($request->port) ? $request->port : '';
        $user->user_name    = $request->user_name;
        $user->password     = $request->password;
        $user->from_name  = $request->from_name;
        $user->reply_email  = $request->reply_email;        
        $user->save();
        }else{
        $user = SmtpSettings::first();         
        $user->server_name = $request->server_name;
        $user->port  = ($request->port) ? $request->port : '';
        $user->user_name    = $request->user_name;
        $user->password     = $request->password;
        $user->from_name  = $request->from_name;
        $user->reply_email  = $request->reply_email;
        $user->save();
        }
        return redirect('admin/smtpsettings')->with('success','Updated sucessfully.');
    }

}