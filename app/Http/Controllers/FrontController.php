<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Hash;
use Validator;
use Auth;
use Carbon\Carbon;
use App\Models\FollowingServices;
use RealRashid\SweetAlert\Facades\Alert;

class FrontController extends Controller
{
    public function index(){
        return redirect('admin');
        return view('welcome');
    } 
}