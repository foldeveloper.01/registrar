<?php

namespace App\Helpers;
use App\Models\Sociallink;
use App\Models\SmtpSettings;

class Helper {

    public static function getSMTPdata(){      
       $results=1;
       $getemaiData = SmtpSettings::count();
       if($getemaiData==0){
            return $results=0;  
       }
       $data = SmtpSettings::first();
       if($data->server_name=='' || $data->port==''|| $data->user_name=='' || $data->password=='' || $data->from_name=='' || $data->reply_email==''){
            return $results=0;
       }
         //Set the data in an array variable from settings table
        $mailConfig = [
            'transport' => 'smtp',
            'host' => $data->server_name,
            'port' => $data->port,
            'encryption' => 'tls',
            'username' => $data->user_name,
            'password' => $data->password,
            'timeout' => null
        ];
        $mailConfig1 = [
            'address' => $data->reply_email,
            'name' => $data->from_name
        ];

        //To set configuration values at runtime, pass an array to the config helper
        config(['mail.mailers.smtp' => $mailConfig]);
        config(['mail.from' => $mailConfig1]);
       return $results;
     }
}