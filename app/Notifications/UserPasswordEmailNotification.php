<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Models\Admin;

class UserPasswordEmailNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Admin $user, $password)
    {
       $this->user = $user;
       $this->password = $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notifiable->email=$this->user->email;        
        return (new MailMessage)
                    ->subject(''.env('APP_NAME').' User Login Details')
                    ->greeting('Hello '.$this->user->firstname.',')
                    ->line('You may now log in to '.env('APP_NAME').'  using the following username and password:')
                    ->line('Username: '.$this->user->email.'')
                    ->line('Password: '.$this->password.'')
                    ->action('Login URL', url('admin'))
                    ->line('Please keep these login details strictly confidential and delete this email once you have noted down your login details.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
